import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-grid>
        <gt-grid-column auto>column #1</gt-grid-column>
        <gt-grid-column>column #2</gt-grid-column>
    </gt-grid>
    
    <gt-grid collapse>
        <gt-grid-column auto>column #1</gt-grid-column>
        <gt-grid-column>column #2</gt-grid-column>   
    </gt-grid>

    <gt-grid collapse.bind="true">
        <gt-grid-column auto>column #1</gt-grid-column>
        <gt-grid-column>column #2</gt-grid-column>   
    </gt-grid>`;
  }
}
