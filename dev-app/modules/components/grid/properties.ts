import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'collapse',
      value: false,
      type: Boolean,
      description: 'removes the grid gap entirely if <code>true</code>',
    },
  ];
}
