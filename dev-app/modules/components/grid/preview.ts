import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public size: number = 6;
  public isCollapsed: boolean = false;
}
