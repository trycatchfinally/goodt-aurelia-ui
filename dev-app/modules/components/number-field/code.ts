import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-number-field
        value.two-way="value"
        step.bind="step"
        skip-steps.bind="skipSteps"
        has-controls.bind="true"
        min.bind="minValue"
        max.bind="maxValue"></gt-number-field>`;
  }

  get js() {
    return `
    export class Demo {
      
      value;
      minValue = -20.5;
      maxValue = 32.5;
      step = 0.5;
      skipSteps = 5;

      bind() {
        this.value = Math.floor(this.maxValue - (this.maxValue - this.minValue) / 2);
      }
    }`;
  }
}
