import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public value: number = 6;

  public isEnabled: boolean = true;
  public hasControls: boolean = true;
  public hasMinValue: boolean = false;
  public hasMaxValue: boolean = false;
  public hasCustomStyles: boolean = false;

  public minValue: number = -20.5;
  public maxValue: number = 32.5;

  public step: number = 0.5;
  public skipSteps: number = 5;

  bind() {
    this.init();
  }

  public init() {
    this.value = Math.floor(this.maxValue - (this.maxValue - this.minValue) / 2);
  }

  get disabled(): boolean {
    return !this.isEnabled;
  }
}
