import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'value',
      type: Number,
      description: 'input value',
    },
    {
      name: 'min',
      type: Number,
      description: 'the minimum numeric value',
    },
    {
      name: 'max',
      type: Number,
      description: 'the maximum numeric value',
    },
    {
      name: 'step',
      value: 1,
      type: Number,
      description:
        'works with <code>min</code> and <code>max</code> to limit the increments at which a numeric value can be set',
    },
    {
      name: 'skipSteps',
      value: 10,
      type: Number,
      description: '<code>step</code> multiplier, applicable for button controls if long press gesture was detected',
    },
    {
      name: 'hasControls',
      value: false,
      type: Boolean,
      description: 'optional buttons to change value by <code>step</code>',
    },
    {
      name: 'inputClass',
      type: String,
      description: 'css classes for input field',
    },
    {
      name: 'ctrlClass',
      type: String,
      description: 'css classes for control buttons',
    },
  ];
}
