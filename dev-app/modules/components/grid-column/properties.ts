import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'auto',
      value: false,
      type: Boolean,
      description: 'a column only takes the space it needs if <code>true</code>',
    },
    {
      name: 'mode',
      value: 12,
      type: Number,
      description:
        'grid can be divided into 12 or 24 columns or may be adaptable: <code>0</code> | <code>12</code> | <code>24</code>',
    },
    {
      name: 'size',
      value: 0,
      type: Number,
      description: 'context value, depends on <code>mode</code> property',
    },
  ];
}
