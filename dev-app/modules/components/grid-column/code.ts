import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-grid>
        <gt-grid-column auto>.col.col-auto</gt-grid-column>
        <gt-grid-column>.col</gt-grid-column>
    </gt-grid>
    
    <gt-grid>
        <gt-grid-column mode="24" size="6">.col.col-6-24</gt-grid-column>
        <gt-grid-column mode="24" size="18">.col.col-18-24</gt-grid-column>
    </gt-grid>
    
    <gt-grid>
        <gt-grid-column mode.bind="12" size.bind="5">.col.col-5-12</gt-grid-column>
        <gt-grid-column mode.bind="12" size.bind="7">.col.col-7-12</gt-grid-column>
    </gt-grid>`;
  }
}
