import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isAuto: boolean = false;
  public isWidthAligned: boolean = false;

  public selectedMode: number = 12;

  public modes: Map<string, number> = new Map([
    ['percents', 0],
    ['12 columns', 12],
    ['24 columns', 24],
  ]);
  public size = {
    second: 0,
    third: 0,
  };

  public firstEl: Element;
  public secondEl: Element;
  public thirdEl: Element;

  created() {
    this.update();
  }

  public update() {
    if (this.isWidthAligned) {
      this.size.second = 0;
      this.size.third = 0;
      this.selectedMode = 0;
    } else {
      let modes = [...this.modes.values()];
      switch (this.selectedMode) {
        case modes[0]:
          this.size.second = 33;
          this.size.third = 50;
          break;
        case modes[1]:
          this.size.second = 4;
          this.size.third = 6;
          break;
        case modes[2]:
          this.size.second = 8;
          this.size.third = 12;
          break;
      }
    }
  }

  get firstElClasses(): string {
    return Preview.classesByEl(this.firstEl);
  }

  get secondElClasses(): string {
    return Preview.classesByEl(this.secondEl);
  }

  get thirdElClasses(): string {
    return Preview.classesByEl(this.thirdEl);
  }

  private static classesByEl(el: Element): string {
    return `.${Array.from(el.classList)
      .filter((x) => x.indexOf('col') === 0)
      .sort()
      .join('.')}`;
  }
}
