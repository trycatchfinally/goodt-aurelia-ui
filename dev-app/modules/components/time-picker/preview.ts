import { computedFrom } from 'aurelia-framework';
// @ts-ignore
import { INameValue } from 'resources/interfaces';

import { PickerPreview } from '../picker-preview';

export class Preview extends PickerPreview {
  public hasCurrentTimeControl: boolean = true;
  public hasResetControl: boolean = false;
  public hasExcluded: boolean = false;
  public presetsOnly: boolean = false;
  public inputOnly: boolean = false;
  public showSeconds: boolean = false;
  public isDisabled: boolean = false;
  public isReadonly: boolean = false;
  public isRequired: boolean = false;
  public time: Date = new Date(new Date().getFullYear(), 0, 0, 14, 0, 0, 0);
  public selectedLocale: string;
  public selectedStep: number;
  public selectedMin: string;
  public selectedMax: string;
  public excludedValues: string[];

  public steps: INameValue[];
  public minValues: INameValue[];
  public maxValues: INameValue[];
  public returnedValue: string = '';

  bind() {
    super.bind();
    this.showCalendarsControl = false;

    const steps = [10, 15, 30, 60];
    const minValues = ['00:00', '10:00', '12:00'];
    const maxValues = ['11:00', '18:00', '21:00', '23:59'];

    this.excludedValues = ['09:30', '11:00', '13:15', '20:00'];
    this.selectedStep = steps.slice(-1).pop();
    this.selectedMin = minValues[0];
    this.selectedMax = maxValues.slice(-1).pop();

    this.steps = steps.map((_) => {
      return { name: `${_} min.`, value: _ };
    });
    this.minValues = minValues.map((_) => {
      return { name: _, value: _ };
    });
    this.maxValues = maxValues.map((_) => {
      return { name: _, value: _ };
    });
  }

  updateTimeSelected = (e: CustomEvent) => {
    this.returnedValue = (e.detail.value as any[])?.length ? JSON.stringify(e.detail.value) : '';
  }

  @computedFrom('hasExcluded', 'excludedValues')
  get excluded(): string[] {
    return this.hasExcluded ? this.excludedValues : [];
  }
}
