// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-time-picker
      value.bind="time"
      has-current-time-control.bind="hasCurrentTimeControl"
      locale.bind="locale"
      show-seconds.bind="false"
      step.bind="step"
      presets-only.bind="false"
      input-only.bind="false"
      min.bind="min"
      max.bind="max"
      excluded.bind="excluded"
      ${ComponentEvents.VALUE_CHANGED}.delegate="triggerTimeUpdate($event)"
    ></gt-time-picker>`;
  }

  get js() {
    return `
    export class Demo {
      
      hasCurrentTimeControl = true;
      time;
      step = 30;
      min = '08:00';
      max = '22:59';
      excluded = ['09:30', '13:00', '13:30', '14:00'];
      locale = 'en-US';

      bind() {
        this.time =  new Date(new Date().getFullYear(), 0, 0, 15, 0, 0, 0);
      }

      triggerTimeUpdate = (e: CustomEvent) => {
        console.log(e.detail.value);
      }
    }`;
  }
}
