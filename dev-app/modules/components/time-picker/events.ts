// @ts-ignore
import { ComponentEvents, ErrorsCodes } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.VALUE_CHANGED,
      description: `returns details with <code>Intl.DateTimeFormatPart[]</code><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/formatToParts" target="_blank"><span class="icon"><i class="mdi mdi-open-in-new"></i></span></a> data, e.g. <code>${ComponentEvents.VALUE_CHANGED}.delegate="triggerTimeUpdate($event.detail.value)"</code>`,
    },
    {
      name: ComponentEvents.ERROR_DETECTED,
      description: `returns details with boolean <code>status</code>, numeric <code>code</code> and <code>description</code>, e.g. <code>${ComponentEvents.ERROR_DETECTED}.delegate="updateErrorStatus($event.detail.error)"</code>
      <br><br>error codes:
      <br><code>${ErrorsCodes.INVALID_VALUE}</code> - invalid time value
      <br><code>${ErrorsCodes.MIN_MAX}</code> - min value can not be greater then max
      <br><code>${ErrorsCodes.OUT_OF_RANGE}</code> - out of min/max range
      <br><code>${ErrorsCodes.NOT_EXCLUDED}</code> - value must be excluded
      `,
    },
  ];
}
