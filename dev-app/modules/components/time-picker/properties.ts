import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'value',
      type: Date,
      description: 'input value, can be initialized as <code>Date</code>',
    },
    {
      name: 'locale',
      type: String,
      description:
        '<code>BCP 47</code> language code <a href="https://www.unicode.org/reports/tr35/tr35.html#BCP_47_Conformance" target="_blank"><span class="icon"><i class="mdi mdi-open-in-new"></i></span></a>, e.g. <code>en-CA</code>, <code>ar-SA</code> and so on',
    },
    {
      name: 'fallbackLocale',
      type: String,
      value: 'en-US',
      description: 'fallback value if <code>locale</code> was not applied',
    },
    {
      name: 'min',
      value: '00:00',
      type: String,
      description: 'the minimum <code>hh:mm</code> string value, a leading zero may be required',
    },
    {
      name: 'max',
      value: '23:59',
      type: String,
      description: 'the maximum <code>hh:mm</code> string value, a leading zero may be required',
    },
    {
      name: 'step',
      value: 60,
      type: Number,
      description: 'minutes value for dropdown',
    },
    {
      name: 'showSeconds',
      value: false,
      type: Boolean,
      description: 'seconds available if <code>true</code>',
    },
    {
      name: 'presetsOnly',
      value: false,
      type: Boolean,
      description: 'readonly mode, only predefined values from dropdown are available',
    },
    {
      name: 'inputOnly',
      value: false,
      type: Boolean,
      description: 'dropdown with preset values is disabled if <code>true</code>',
    },
    {
      name: 'hasCurrentTimeControl',
      value: false,
      type: Boolean,
      description: 'current time value can be set by control if <code>true</code>',
    },
    {
      name: 'hasResetControl',
      value: false,
      type: Boolean,
      description: 'reset control, available if <code>true</code>',
    },
    {
      name: 'excluded',
      type: Array,
      description: 'excluded <code>hh:mm</code> string values, a leading zero may be required per value',
    },
  ];
}
