// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'type',
      type: String,
      description:
        'type of proper styling, possible values: <code>success</code> | <code>warn</code> | <code>error</code>',
    },
    {
      name: 'wrapperClass',
      type: String,
      description: 'css classes for alert wrapper',
    },
    {
      name: 'isCloseable',
      value: true,
      type: Boolean,
      description: `alert can be closed if <code>true</code>, see <code>${ComponentEvents.DEACTIVATE_READY}</code> event for complete removal`,
    },
  ];
}
