// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-alert>
        <h4>Title</h4>
        <div>Body content</div>
    </gt-alert>

    <gt-alert type.bind="'warn'" is-closeable.bind="false">
        <h4>Title</h4>
        <div>Body content</div>
    </gt-alert>
    
    <gt-alert type.bind="type" 
              wrapper-class.bind="wrapperClass" 
              if.bind="active" 
              ${ComponentEvents.DEACTIVATE_READY}.delegate="removeTrigger($event)">
        <h4>Title</h4>
        <div>Body content</div>
    </gt-alert>`;
  }

  get js() {
    return `
    export class Demo {
      
      active = true;
      type = 'success';
      wrapperClass = 'mar-top-4';
      
      removeTrigger(event) {
        this.active = false;
      }
    }`;
  }
}
