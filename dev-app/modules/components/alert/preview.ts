import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public wrapperClass: string = 'mar-top-5';
  public types: string[] = ['success', 'warn', 'error'];
  public active: boolean[] = Array(this.types.length).fill(true);

  removeTrigger(e: CustomEvent, index: number) {
    this.active[index] = false;
    this.active = this.active.slice();
  }
}
