import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-switch model.bind="product.id" 
               state.bind="selectedState"
               is-small.bind="false"
               active.bind="selectedProductIds"></gt-switch>
    `;
  }

  get js() {
    return `
    export class Demo {
      
      products = [
        { id: 0, name: 'Motherboard' },
        { id: 1, name: 'CPU' },
        { id: 2, name: 'Memory' },
      ];
      states = ['hover', 'focus', 'disabled', 'valid', 'invalid'];

      selectedProductIds = [];
      selectedState;

      created() {
          this.selectedState = this.states[0];
      }
    }`;
  }
}
