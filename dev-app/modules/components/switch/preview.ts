import { PreviewBase } from '../preview-base';

export interface IProduct {
  id: number;
  name: string;
}

export class Preview extends PreviewBase {
  public states: string[] = ['hover', 'focus', 'disabled', 'valid', 'invalid'];
  public products: IProduct[] = [
    { id: 0, name: 'Motherboard' },
    { id: 1, name: 'CPU' },
    { id: 2, name: 'Memory' },
  ];

  public isSmall: boolean = false;
  public selectedState: string = this.states[0];
  public selectedProductIds: number[] = [];
}
