import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'model',
      type: Object,
      description: 'any type of data to pass model',
    },
    {
      name: 'active',
      value: false,
      type: Boolean,
      description: 'active or checked if <code>true</code>',
    },
    {
      name: 'isSmall',
      value: false,
      type: Boolean,
      description: 'component is small if <code>true</code>',
    },
    {
      name: 'state',
      type: String,
      description:
        'for proper state and styling, possible values:<br><code>hover</code> | <code>focus</code> | <code>disabled</code> |  <code>valid</code> |  <code>invalid</code>',
    },
  ];
}
