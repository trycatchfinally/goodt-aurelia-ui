import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isActive: boolean = false;
  public isRight: boolean = true;
}
