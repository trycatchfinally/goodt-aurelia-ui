import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-off-canvas active is-right>
      <p>Off-canvas content</p>
    </gt-off-canvas>
    
    <gt-off-canvas active.two-way="offCanvasActive" is-right.bind="true">
      <p>Off-canvas content</p>
    </gt-off-canvas>`;
  }

  get js() {
    return `
    export class Demo {
      
      offCanvasActive = true;
      
    }`;
  }
}
