import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'active',
      value: false,
      type: Boolean,
      description: 'state of the component, off-canvas menu is visible if <code>true</code>',
    },
    {
      name: 'isRight',
      value: false,
      type: Boolean,
      description: 'right side positioning',
    },
  ];
}
