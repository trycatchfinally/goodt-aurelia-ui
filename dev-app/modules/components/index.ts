import { IConfig } from '../../types';

export default new Map<string, IConfig>([
  [
    'alert',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-alert',
      properties: true,
      events: true,
    },
  ],
  [
    'avatar',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
    },
  ],
  [
    'checkbox',
    {
      preview: true,
      code: true,
      description: true,
      url: '#form/form-checkbox',
      properties: true,
    },
  ],
  [
    'chip',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
      events: true,
    },
  ],
  [
    'date-picker',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
      events: true,
    },
  ],
  [
    'details',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
    },
  ],
  [
    'dropdown',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-dropdown',
      properties: true,
      events: true,
    },
  ],
  [
    'grid',
    {
      preview: true,
      code: true,
      description: true,
      url: '#grid',
      properties: true,
    },
  ],
  [
    'grid-column',
    {
      preview: true,
      code: true,
      description: true,
      url: '#grid/grid-col',
      properties: true,
    },
  ],
  [
    'menu',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-menu',
      properties: true,
      events: false,
    },
  ],
  [
    'number-field',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
      events: false,
    },
  ],
  [
    'off-canvas',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-offcanvas',
      properties: true,
    },
  ],
  [
    'popup',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-popup',
      properties: true,
      events: true,
    },
  ],
  [
    'preloader',
    {
      preview: true,
      code: true,
      description: false,
      url: '#components/components-preloader',
      properties: true,
    },
  ],
  [
    'radio',
    {
      preview: true,
      code: true,
      description: true,
      url: '#form/form-radio',
      properties: true,
    },
  ],
  [
    'select',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
    },
  ],
  [
    'shim',
    {
      preview: true,
      code: true,
      description: false,
      url: '#components/components-shim',
      properties: true,
    },
  ],
  [
    'switch',
    {
      preview: true,
      code: true,
      description: true,
      url: '#form/form-switch',
      properties: true,
    },
  ],
  [
    'tabs',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-tabs',
      properties: true,
      events: true,
      validSlot: 'GtTab',
    },
  ],
  [
    'time-picker',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
      events: true,
    },
  ],
  [
    'toast',
    {
      preview: true,
      code: true,
      description: true,
      url: '#components/components-toast',
      properties: true,
    },
  ],
  [
    'tree',
    {
      preview: true,
      code: true,
      description: true,
      properties: true,
      events: true,
    },
  ],
]);
