import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-checkbox
      repeat.for="product of products"
      is-small.bind="isSmall"
      is-partial.bind="isPartial"
      disabled.bind="isDisabled"
      change.delegate="selectProduct($event, product)"
      name.bind="inputName"
      checked.bind="value"
      class.bind="isRight && 'pull-right'"
      class="mar-right-6"
      model.bind="product.value">
      <span
        class="mar-top-1"
        class.bind="isRight ? 'mar-right-3 pull-left' : 'mar-left-3 pull-right'">
          $\{product.name\}
      </span>
    </gt-checkbox>`;
  }

  get js() {
    return `
    export class Demo {

      isSmall = true;
      isRight = false;
      isPartial = false;
      isDisabled = false;
      inputName = 'checkbox-group-example';
      value = [1, 3, 5];

      products = [
        { value: 0, name: 'Motherboard' },
        { value: 1, name: 'CPU' },
        { value: 2, name: 'Memory' },
        { value: 3, name: 'Drives' },
        { value: 4, name: 'Display' },
        { value: 5, name: 'Keyboard' }
      ];

      selectProduct(event, product) {
        if (!event.target.checked) {
          this.value = this.value.filter(value => value != product.value);
        }
        else {
          this.value.push(product.value);
        }
        return true;
      }
    }`;
  }
}
