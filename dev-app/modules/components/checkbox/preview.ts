import { IProduct, products } from '../../../data/products';
import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public inputName: string = 'example-checkbox';
  public value: number[] = [1, 3, 5];

  public isSmall: boolean = true;
  public isPartial: boolean = false;
  public isRight: boolean = false;
  public isDisabled: boolean = false;

  public products: IProduct[] = products;

  selectProduct(event: Event, product: IProduct): boolean {
    if (!(event.target as HTMLInputElement).checked) {
      this.value = this.value.filter((value: any) => value != product.value);
    } else {
      this.value.push(product.value);
    }
    return true;
  }

  get selectedProductName(): string[] {
    return this.products
      .filter((p) => this.value.includes(p.value))
      .map((p) => p.name)
      .sort();
  }
}
