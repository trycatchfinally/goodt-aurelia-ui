import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'name',
      type: String,
      required: true,
      description: 'the checkbox group must have share the same <code>name</code> to be treated as a group',
    },
    {
      name: 'isPartial',
      type: Boolean,
      value: false,
      description: 'checkbox displayed as partially selected if <code>true</code>',
    },
  ];
}
