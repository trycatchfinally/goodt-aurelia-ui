import { IConfigProperty } from '../../../types';
import { EXTERNAL_BASE_URL } from '../../component-details';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'inverse',
      value: false,
      type: Boolean,
      description: 'inverts color if &nbsp;<code>color</code>&nbsp; property is not defined',
    },
    {
      name: 'size',
      type: Number,
      description: `preloader size, use <code>@spacer</code> values<a href="${EXTERNAL_BASE_URL}#about/about-spacers" target="_blank"><span class="icon"><i class="mdi mdi-open-in-new"></i></span></a>`,
    },
    {
      name: 'layout-size',
      type: Number,
      description: `preloader size, use <code>@spacer-layout</code> values<a href="${EXTERNAL_BASE_URL}#about/about-spacers" target="_blank"><span class="icon"><i class="mdi mdi-open-in-new"></i></span></a>`,
    },
    {
      name: 'color',
      type: String,
      description: `value for &nbsp;<code>.bg-{color}</code>&nbsp; styling class`,
    },
  ];
}
