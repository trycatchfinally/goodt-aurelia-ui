import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-preloader></gt-preloader>
    
    <gt-preloader layout-size.bind="layoutSize"></gt-preloader>

    <gt-preloader color.bind="selectedColor" 
                  size.bind="size"></gt-preloader>`;
  }

  get js() {
    return `
    export class Demo {
    
      inverse = false;
      selectedColor = 'green';
      size = 7;
      layoutSize = 5;
      
    }`;
  }
}
