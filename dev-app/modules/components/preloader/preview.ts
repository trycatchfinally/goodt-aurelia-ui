import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public inverse: boolean = false;
  public layoutSize: number = 0;
  public size: number = 0;
  public selectedColor: string;

  public maxLayoutSize: number = 5;
  public maxSize: number = 7;
  public colors: string[] = ['none', 'primary', 'green', 'yellow', 'orange', 'red'];

  created() {
    this.selectedColor = this.colors[0];
  }

  public resetProp = (prop: string) => (this[prop] = 0);

  get selectedColorValue(): boolean | string {
    return this.selectedColor === 'none' ? false : this.selectedColor;
  }
}
