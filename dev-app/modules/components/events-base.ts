import { useView } from 'aurelia-framework';

import { IConfigEvent } from '../../types';

@useView('modules/components/events.html')
export class EventsBase {
  protected items: IConfigEvent[] = [];
}
