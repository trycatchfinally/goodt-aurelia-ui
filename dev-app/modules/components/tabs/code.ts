// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-tabs is-centered grow 
             active-tab-id.bind="defaultTabId"
             ${ComponentEvents.ITEM_SELECT}.delegate="onTabSelect($event.detail)">
      <gt-tab header="Star Trek">
        <ul>
          <li>The Original Series</li>
          <li>he Animated Series</li>
          <li>The Next Generation</li>
          <li>Deep Space Nine</li>
          <li>Voyager</li>
          <li>Enterprise</li>
          <li>Discovery</li>
          <li>Picard</li>
        </ul>
      </gt-tab>
      <gt-tab header="Star Wars">
        <ul>
          <li>Episode IV – A New Hope</li>
          <li>Episode V – The Empire Strikes Back</li>
          <li>Episode VI – Return of the Jedi</li>
          <li>Episode I – The Phantom Menace</li>
          <li>Episode II – Attack of the Clones</li>
          <li>Episode III – Revenge of the Sith</li>
          <li>Episode VII – The Force Awakens</li>
          <li>Episode VIII – The Last Jedi</li>
          <li>Episode IX – The Rise of Skywalker</li>
        </ul>
      </gt-tab>
      <gt-tab header="Battlestar Galactica">
        <ul>
          <li>Battlestar Galactica</li>
          <li>Razor</li>
          <li>The Plan</li>
          <li>Caprica</li>
          <li>Blood & Chrome</li>
        </ul>
      </gt-tab>
    </gt-tabs>`;
  }

  get js() {
    return `
    export class Demo {
    
      defaultTabId = 2;
      
      onTabSelect({index}) {
        console.log(index);
      }
    }`;
  }
}
