import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'activeTabId',
      type: Number,
      description: 'index of active tab',
    },
    {
      name: 'isCentered',
      value: false,
      type: Boolean,
      description: 'center alignment',
    },
    {
      name: 'isRight',
      value: false,
      type: Boolean,
      description: 'right alignment',
    },
    {
      name: 'grow',
      value: false,
      type: Boolean,
      description: 'tabs width is full if <code>true</code>',
    },
  ];
}
