// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.ITEM_SELECT,
      description: `returns details with index id <code>$event.detail.id</code> for tab selected, e.g. <code>${ComponentEvents.ITEM_SELECT}.delegate="onTabSelect($event.detail)"</code>`,
    },
  ];
}
