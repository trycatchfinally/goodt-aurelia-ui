import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isRight: boolean = false;
  public isCentered: boolean = false;
  public isGrown: boolean = false;

  public selectedTab: number = 1;
  public selectedMode: string;

  public modes: Map<string, string> = new Map([
    ['default', 'default'],
    ['right', 'isRight'],
    ['centered', 'isCentered'],
  ]);

  created() {
    this.selectedMode = [...this.modes.values()][0];
  }

  update() {
    this.isRight = this.isCentered = false;
    if (this.selectedMode !== [...this.modes.values()][0]) {
      this[this.selectedMode] = true;
    }
  }

  public onTabSelect(data): void {
    this.selectedTab = data?.id;
  }
}
