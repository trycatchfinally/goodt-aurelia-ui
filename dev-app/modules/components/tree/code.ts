// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-tree header="item 1" item-id="100">
      <gt-tree header="item 1.1" item-id="110"></gt-tree>
      <gt-tree header.bind="'item 1.2'" item-id.bind="120">
        <gt-tree header="item 1.2.1" item-id="121"></gt-tree>
      </gt-tree>
    </gt-tree>
    <gt-tree header="item 2" 
             item-id="200"
             initialized.two-way="initialized"
             ${ComponentEvents.ITEM_SELECT}.delegate="onItemSelect($event.detail)"
             ${ComponentEvents.ITEM_CTRL_UPDATE}.delegate="onCtrlUpdate($event.detail)">
      <gt-tree header="item 2.1">
        <gt-tree header="item 2.1.1"></gt-tree>
      </gt-tree>
    </gt-tree>`;
  }

  get js() {
    return `
    export class Demo {
      
      initialized = false;
      
      onItemSelect({id}) {
        console.log(id)
      }
    
      onCtrlUpdate({id, status, initialized}) {
        console.log(id, status, initialized)
      }
    }`;
  }
}
