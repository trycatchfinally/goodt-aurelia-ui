// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { PreviewBase } from '../preview-base';

interface ITree {
  header: string;
  id: number;
  initialized: boolean;
  children?: ITree[];
}

export class Preview extends PreviewBase {
  public selectedItemId: any;
  public selectedItemStatus: boolean = false;
  public isInitialized: boolean = true;
  public lastDetectedEvent: any;

  public initialized: boolean = false;

  public primaryRoot: ITree;
  public secondaryRoot: ITree;

  created() {
    this.primaryRoot = {
      header: 'item 4',
      id: 400,
      initialized: false,
      children: [],
    };
    this.secondaryRoot = {
      header: 'item 5',
      id: 500,
      initialized: false,
      children: [],
    };
    const children = new Array(4).fill(0).map((v, k) => {
      return {
        header: `item 4.${k + 1}`,
        id: (40 + k + 1) * 10,
        initialized: false,
        children: [],
      };
    });
    setTimeout(() => {
      children.map((item) => (item.initialized = true));
      this.primaryRoot.children = children;
      this.primaryRoot.initialized = true;
      this.secondaryRoot.initialized = true;

      this.initialized = true;
    }, 2000);
  }

  public onItemSelect({ id }) {
    this.selectedItemId = id;
    this.lastDetectedEvent = ComponentEvents.ITEM_SELECT;
  }

  public onCtrlUpdate({ id, status, initialized }) {
    this.selectedItemId = id;
    this.selectedItemStatus = status;
    this.isInitialized = initialized;
    this.lastDetectedEvent = ComponentEvents.ITEM_CTRL_UPDATE;
  }
}
