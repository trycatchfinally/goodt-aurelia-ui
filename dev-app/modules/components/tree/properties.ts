// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'header',
      type: String,
      description: 'item readable label',
    },
    {
      name: 'itemId',
      type: Number,
      description: 'unique item identifier',
    },
    {
      name: 'active',
      value: false,
      type: Boolean,
      description: 'item is open if <code>true</code>',
    },
    {
      name: 'initialized',
      value: true,
      type: Boolean,
      description: `property is used for asynchronous initialization, e.g. subitems loading completion<br>item can be opened if <code>true</code><br>see also <code>${ComponentEvents.ITEM_CTRL_UPDATE}</code> event`,
    },
    {
      name: 'iconClass',
      type: String,
      description: 'css classes for item icon',
    },
    {
      name: 'labelClass',
      type: String,
      description: 'css classes for item label',
    },
  ];
}
