// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.ITEM_SELECT,
      description: `returns details with item <code>id</code> for item clicked, e.g. <code>${ComponentEvents.ITEM_SELECT}.delegate="onItemSelect($event.detail)"</code>`,
    },
    {
      name: ComponentEvents.ITEM_CTRL_UPDATE,
      description: `returns details for item toggled, e.g. <code>${ComponentEvents.ITEM_CTRL_UPDATE}.delegate="onCtrlUpdate($event.detail)"</code><br>returned props are <code>id</code> as number, <code>status</code> as boolean, <code>initialized</code> as boolean`,
    },
  ];
}
