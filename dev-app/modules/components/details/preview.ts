import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isRight: boolean = true;
  public active: boolean = true;
}
