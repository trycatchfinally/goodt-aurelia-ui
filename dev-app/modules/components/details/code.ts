import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-details
      class="w-100"
      summary="Summary text"
      is-right.bind="isRight"
      active.two-way="active"
      summary-class="text-bold"
      icon-class.bind="active && 'color-primary'">
        A disclosure widget is typically presented onscreen using a small triangle
        which rotates or twists to indicate open/closed status, with a label next
        to the triangle.
    </gt-details>`;
  }

  get js() {
    return `
    export class Demo {
    
      isRight = true;
      active = true;
      
    }`;
  }
}
