import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'active',
      type: Boolean,
      value: false,
      description: 'detailed information is visible if&nbsp;<code>true</code>',
    },
    {
      name: 'isRight',
      type: Boolean,
      value: false,
      description: 'summary icon positioning',
    },
    {
      name: 'summary',
      type: String,
      description: 'summary text',
    },
    {
      name: 'iconClass',
      type: String,
      description: 'css classes for item icon',
    },
    {
      name: 'summaryClass',
      type: String,
      description: 'css classes for summary text',
    },
  ];
}
