// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-dropdown items.bind="items"
                 active="2"></gt-dropdown>

    <gt-dropdown ${ComponentEvents.ITEM_SELECT}.delegate="onItemSelect($event.detail)"
                 disabled.bind="disabled"
                 active.bind="active"
                 items.bind="items"></gt-dropdown>`;
  }

  get js() {
    return `
    export class Demo {
      
      items = ['Um', 'Dois', 'Tres', 'Quatro', 'Cinco', 'Seis'];
      disabled = [3, 5];
      active = 1;
      
      onItemSelect(data) {
        this.active = data.index;
      }
    }`;
  }
}
