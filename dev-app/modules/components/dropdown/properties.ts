import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'items',
      type: Array,
      description: 'array of items with string values listed in dropdown',
    },
    {
      name: 'active',
      type: Number,
      description: 'index of selected item',
    },
    {
      name: 'disabled',
      type: Array,
      description: 'disabled items by array of indexes',
    },
  ];
}
