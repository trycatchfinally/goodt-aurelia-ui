// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.ITEM_SELECT,
      description: `returns details with <code>index</code> for item selected, e.g. <code>${ComponentEvents.ITEM_SELECT}.delegate="onItemSelect($event.detail)"</code>`,
    },
  ];
}
