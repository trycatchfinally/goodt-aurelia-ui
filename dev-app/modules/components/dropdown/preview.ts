import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public items: string[] = ['Um', 'Dois', 'Tres', 'Quatro', 'Cinco', 'Seis'];
  public disabled = [3, 5];
  public active = 1;

  onItemSelect(data) {
    this.active = data?.index;
  }
}
