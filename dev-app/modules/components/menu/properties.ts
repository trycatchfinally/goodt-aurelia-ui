import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'isSmall',
      value: false,
      type: Boolean,
      description: 'menu is small if <code>true</code>',
    },
    {
      name: 'isLeft',
      value: false,
      type: Boolean,
      description: 'left alignment for <strong>submenus</strong>',
    },
  ];
}
