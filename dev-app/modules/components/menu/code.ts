import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-menu is-small>
      <ul>
        <li class="active"><a href="#">Active</a></li>
        <li class="disabled"><a href="#">Disabled</a></li>
        <li><hr></li>
        <li>
          <a href="#">Submenu</a>
          <gt-menu is-small is-left>
            <ul>
              <li><a href="#">Item</a></li>
              <li>
                <a href="#">Submenu</a>
                <gt-menu is-small.bind="isMenuSmall"
                         is-left.bind="isMenuLeft">
                  <ul>
                    <li><a href="#">Item</a></li>
                    <li><a href="#">Item</a></li>
                  </ul>
                </gt-menu>
              </li>
            </ul>
          </gt-menu>
        </li>
      </ul>
    </gt-menu>`;
  }

  get js() {
    return `
    export class Demo {
    
      isMenuSmall = true;
      isMenuLeft = true;
    
    }`;
  }
}
