import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isLeft: boolean = false;
  public isSmall: boolean = false;

  menuWrapperEl: Element;

  attached() {
    this.innerLinks.forEach((link) => {
      link.addEventListener('click', Preview.clickTrigger);
    });
  }

  detached() {
    this.innerLinks.forEach((link) => {
      link.removeEventListener('click', Preview.clickTrigger);
    });
  }

  private static clickTrigger(e) {
    e.preventDefault();
  }

  get innerLinks() {
    return this.menuWrapperEl.querySelectorAll('a[href="#"]');
  }
}
