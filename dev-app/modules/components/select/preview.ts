import { IProduct, products } from '../../../data/products';
import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public initial: number | number[];
  public isMultiple: boolean = true;
  public isDisabled: boolean = false;
  public hasSummary: boolean = false;
  public hasResetControl: boolean = true;

  public products: IProduct[] = products;

  bind() {
    this.initValues();
  }

  initValues() {
    this.initial = this.isMultiple ? [1, 2, 5] : 2;
  }

  get selectedItems(): string[] {
    let values: any[] = this.isMultiple ? (this.initial as any[]) : [this.initial];
    return this.products
      .filter((product) => values.includes(product.value))
      .map((item) => item.name)
      .sort();
  }
}
