import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'items',
      type: Array,
      description:
        'array of items with <code>name</code>, <code>value</code> properties; <code>name</code> must be a string',
    },
    {
      name: 'name',
      type: String,
      description: 'standard input <code>name</code> attribute, name of the form control, optional',
    },
    {
      name: 'value',
      type: 'Array | String | Number',
      description: 'array of selected values, if <code>multiple</code> mode enabled, or single value',
    },
    {
      name: 'multiple',
      type: Boolean,
      value: false,
      description: 'mutiple values can be selected if <code>true</code>',
    },
    {
      name: 'placeholder',
      type: String,
      description: 'placeholder for input field',
    },
    {
      name: 'wrapperClass',
      type: String,
      description: 'css classes for input field',
    },
    {
      name: 'dropdownClass',
      type: String,
      description: 'css classes for dropdown list',
    },
    {
      name: 'hasResetControl',
      type: Boolean,
      value: true,
      description: 'reset control visible on focused state; valid for <code>multiple</code> mode only',
    },
    {
      name: 'summary',
      type: String,
      value: '{n}',
      description:
        'counter template for selected values, <code>{n}</code> will be replaced with counter; valid for <code>multiple</code> mode only',
    },
    {
      name: 'summary',
      type: Boolean,
      description: 'selected values separated by comma if <code>true</code>; valid for <code>multiple</code> mode only',
    },
  ];
}
