import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-select
      value.two-way="values"
      items.bind="products"
      placeholder="Products"
      summary.bind="hasSummary"
      multiple.bind="isMultiple"
      disabled.bind="false">
    </gt-select>`;
  }

  get js() {
    return `
    export class Demo {

      isMultiple = true;
      hasSummary = true;

      values = [1, 2, 5];
      products = [
        { value: 0, name: 'Motherboard' },
        { value: 1, name: 'CPU' },
        { value: 2, name: 'Memory' },
        { value: 3, name: 'Drives' },
        { value: 4, name: 'Display' },
        { value: 5, name: 'Keyboard' }
      ];
    }`;
  }
}
