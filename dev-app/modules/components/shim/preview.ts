import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isHoverOnly: boolean = false;
  public isChangedBg: boolean = false;
}
