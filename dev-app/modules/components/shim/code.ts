import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-shim hover-only.bind="isHoverOnly"
             overlay-bg-class.bind="isChangedBg && 'bg-green'">
      <h3 class="last">content</h3>
    </gt-shim>`;
  }

  get js() {
    return `
    export class Demo {
    
      isHoverOnly = false;
      isChangedBg = false;
      
    }`;
  }
}
