import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'hoverOnly',
      type: Boolean,
      value: false,
      description: 'shim is active only if&nbsp;<code>true</code>&nbsp; and mouse cursor is over',
    },
    {
      name: 'overlayBgClass',
      type: String,
      description: '<code>.bg-*</code> class for overlay background styling',
    },
  ];
}
