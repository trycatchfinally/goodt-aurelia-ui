// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-date-picker
      value.bind="value"
      has-today-control.bind="hasTodayControl"
      has-reset-control.bind="hasResetControl"
      locale.bind="locale"
      force-gregory.bind="true"
      first-day-of-week.bind="firstDayOfWeek"
      excluded-week-days.bind="excludedWeekDays"
      ${ComponentEvents.VALUE_CHANGED}.delegate="updateDateSelected($event)"
    ></gt-date-picker>`;
  }

  get js() {
    return `
    export class Demo {

      hasTodayControl = true;
      hasResetControl = false;
      locale = 'en-US';
      firstDayOfWeek = 0;
      value = new Date();
      excludedWeekDays = [1,6];

      updateDateSelected = (e: CustomEvent) => {
        console.log(e.detail.value);
      }
    }`;
  }
}
