import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'value',
      type: 'Date | String',
      description:
        'input value, can be initialized as <code>Date</code> <br /><em>Gregorian</em> calendar can also accept <code>yyyy-mm-dd</code> string value',
    },
    {
      name: 'locale',
      type: String,
      description:
        '<code>BCP 47</code> language code <a href="https://www.unicode.org/reports/tr35/tr35.html#BCP_47_Conformance" target="_blank"><span class="icon"><i class="mdi mdi-open-in-new"></i></span></a>, e.g. <code>en-CA</code>, <code>ar-SA</code> and so on',
    },
    {
      name: 'fallbackLocale',
      type: String,
      value: 'en-US',
      description: 'fallback value if <code>locale</code> was not applied',
    },
    {
      name: 'min',
      type: String,
      description: 'the minimum <code>yyyy-mm-dd</code> string value',
    },
    {
      name: 'max',
      type: String,
      description: 'the maximum <code>yyyy-mm-dd</code> string value',
    },
    {
      name: 'firstDayOfWeek',
      value: 0,
      type: Number,
      description:
        'first day of week, acceptable values are <code>0</code>-<code>6</code> <br />where <code>0</code> is Sunday, <code>6</code> is Saturday',
    },
    {
      name: 'forceGregory',
      value: true,
      type: Boolean,
      description: 'calendar always is <em>Gregorian</em> if <code>true</code>',
    },
    {
      name: 'hasTodayControl',
      value: true,
      type: Boolean,
      description: 'today value can be set by control if <code>true</code>',
    },
    {
      name: 'inputOnly',
      value: false,
      type: Boolean,
      description: 'calendar is disabled if <code>true</code>',
    },
    {
      name: 'hasResetControl',
      value: false,
      type: Boolean,
      description: 'reset control, available if <code>true</code>',
    },
    {
      name: 'excludedDates',
      type: Array,
      description: 'dates to exclude from selection, an array of <code>yyyy-mm-dd</code> string values',
    },
    {
      name: 'excludedWeekDays',
      type: Array,
      description:
        'weekdays to exclude from selection, an array of <code>0</code>-<code>6</code> number values where <code>0</code> is Sunday, <code>6</code> is Saturday',
    },
  ];
}
