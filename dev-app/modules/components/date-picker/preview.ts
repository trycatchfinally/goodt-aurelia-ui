import { computedFrom } from 'aurelia-framework';
// @ts-ignore
import { formatDateLabel } from 'resources/components/base/helpers';
// @ts-ignore
import { INameValue } from 'resources/interfaces';

import { PickerPreview } from '../picker-preview';

export class Preview extends PickerPreview {
  public hasTodayControl: boolean = true;
  public hasResetControl: boolean = false;
  public hasExcludedDates: boolean = false;

  public inputOnly: boolean = false;
  public isDisabled: boolean = false;
  public isReadonly: boolean = false;
  public isRequired: boolean = false;
  public firstDayOfWeek: number = 0;

  public value: string | Date;

  public selectedMin: string;
  public selectedMax: string;

  public excludedDates: string[] = [];
  public excludedWeekDays: number[] = [];

  public minValues: INameValue[];
  public maxValues: INameValue[];
  public weekdays: INameValue[];

  bind() {
    super.bind();

    const dayDelta = 60 * 60 * 24 * 1000;
    const today = new Date();
    const weekdays = [...new Array(7)].map((_, i) => i);
    const weekdayFormatter = new Intl.DateTimeFormat('en-US', {
      weekday: 'long',
    });

    this.weekdays = weekdays.map((d) => {
      return {
        name: weekdayFormatter.format(new Date(today).setTime(today.getTime() + (d - today.getDay()) * dayDelta)),
        value: d,
      };
    });
    this.value = today;

    const minValues = [-10, -5, 0].map((y) =>
      formatDateLabel(new Date(new Date(today.getTime()).setFullYear(today.getFullYear() + y))),
    );
    const maxValues = [0, 5, 10].map((y) =>
      formatDateLabel(new Date(new Date(today.getTime()).setFullYear(today.getFullYear() + y))),
    );

    this.excludedWeekDays = [];
    this.excludedDates = [7, 10].map((v) => formatDateLabel(new Date(today.getTime() - v * dayDelta)));

    this.selectedMin = minValues[0];
    this.selectedMax = maxValues.slice(-1).pop();

    this.minValues = minValues.map((_) => {
      return { name: _ ? _ : 'none', value: _ };
    });
    this.maxValues = maxValues.map((_) => {
      return { name: _ ? _ : 'none', value: _ };
    });
  }

  updateDateSelected = (e: CustomEvent) => {
    this.returnedValue = (e.detail.value as any[])?.length ? JSON.stringify(e.detail.value) : '';
  }

  @computedFrom('hasExcludedDates', 'excludedDates')
  get computedExcludedDates(): string[] {
    return this.hasExcludedDates ? this.excludedDates : [];
  }
}
