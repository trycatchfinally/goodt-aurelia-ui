import { PreviewBase } from '../preview-base';
import { imgSource } from './img-source';

interface ISizes {
  min: number;
  max: number;
}

export class Preview extends PreviewBase {
  public url: string = imgSource;
  public currentSize: number;
  public isRounded: boolean = false;
  public hasImage: boolean = true;

  public sizes: ISizes = {
    min: 1,
    max: 7,
  };

  created() {
    this.currentSize = 4;
  }
}
