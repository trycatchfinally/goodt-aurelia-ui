import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-avatar
      url.bind="imgSource"
      rounded.bind="isRounded"
      size.bind="currentSize"></gt-avatar>`;
  }

  get js() {
    return `
    export class Demo {

      imgSource = 'https://api.adorable.io/avatars/250/example@example.com.png';
      currentSize = 4;
      isRounded = false;
      
    }`;
  }
}
