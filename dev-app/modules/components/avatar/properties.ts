import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'url',
      type: String,
      description: 'avatar image source',
    },
    {
      name: 'size',
      type: Number,
      value: 1,
      description: 'avatar size, available values are&nbsp;<code>1</code>-<code>7</code>',
    },
    {
      name: 'rounded',
      type: Boolean,
      value: false,
      description: 'avatar has round borders if <code>true</code>',
    },
    {
      name: 'fallbackIcon',
      type: String,
      value: 'mdi-account-circle',
      description: 'default icon if <code>url</code> is not defined',
    },
  ];
}
