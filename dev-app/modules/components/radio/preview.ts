import { IProduct, products } from '../../../data/products';
import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public inputName: string = 'example-radio';
  public value: number = 3;

  public isSmall: boolean = true;
  public isRight: boolean = false;
  public isDisabled: boolean = false;

  public products: IProduct[] = products;

  selectProduct(product: IProduct): void {
    this.value = product.value;
  }

  get selectedProductName(): string {
    return this.products.find((p) => p.value === this.value).name;
  }
}
