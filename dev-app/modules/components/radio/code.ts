import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-radio
      repeat.for="product of products"
      is-small.bind="isSmall"
      disabled.bind="isDisabled"
      change.delegate="selectProduct(product)"
      name.bind="inputName"
      checked.bind="value"
      class.bind="isRight && 'pull-right'"
      class="mar-right-6"
      model.bind="product.value">
      <span
        class="mar-top-1"
        class.bind="isRight ? 'mar-right-3 pull-left' : 'mar-left-3 pull-right'">
          $\{product.name\}
      </span>
    </gt-radio>`;
  }

  get js() {
    return `
    export class Demo {

      isSmall = true;
      isRight = false;
      isDisabled = false;
      inputName = 'radio-group-example';
      value = 3;

      products = [
        { value: 0, name: 'Motherboard' },
        { value: 1, name: 'CPU' },
        { value: 2, name: 'Memory' },
        { value: 3, name: 'Drives' },
        { value: 4, name: 'Display' },
        { value: 5, name: 'Keyboard' }
      ];
    
      selectProduct(product) {
        this.value = product.value;
      }
    }`;
  }
}
