import { useView } from 'aurelia-framework';

import { IConfigProperty } from '../../types';

@useView('modules/components/properties.html')
export class PropertiesBase {
  protected items: IConfigProperty[] = [];
  public showShortcuts: boolean = true;

  activate(model: any) {
    this.showShortcuts = model?.showShortcuts ?? true;
  }

  get anyBoolean() {
    return this.items.some((v) => v.type?.name?.toLowerCase() === 'boolean');
  }

  get firstBoolean() {
    return this.items.find((v) => v.type?.name?.toLowerCase() === 'boolean')?.name;
  }
}
