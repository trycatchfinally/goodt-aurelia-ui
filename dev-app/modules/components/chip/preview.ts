import { PreviewBase } from '../preview-base';

interface IChipData {
  color: string | boolean;
  value: string;
}

export class Preview extends PreviewBase {
  public chips: IChipData[];
  public isRounded: boolean = false;
  public lastChipRemoved: string;

  private backup: IChipData[];

  created() {
    this.chips = [
      {
        color: false,
        value: 'Um',
      },
      {
        color: 'primary',
        value: 'Dois',
      },
      {
        color: 'success',
        value: 'Tres',
      },
      {
        color: 'warn',
        value: 'Quatro',
      },
      {
        color: 'error',
        value: 'Cinco',
      },
      {
        color: 'misc',
        value: 'Seis',
      },
    ];
    this.backup = [...this.chips];
  }

  public removeTrigger(e: CustomEvent) {
    const value = (e.detail as any)?.value;
    if (this.chips.some((chip) => chip.value === value)) {
      this.lastChipRemoved = value;
      this.chips = this.chips.filter((chip) => chip.value != value);
    }
  }

  public restore() {
    this.chips = [...this.backup];
    this.lastChipRemoved = undefined;
  }
}
