// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.DEACTIVATE_READY,
      description: `returns details with <code>value</code> for chip selected, e.g. <code>${ComponentEvents.DEACTIVATE_READY}.delegate="removeTrigger($event.detail)"</code>`,
    },
  ];
}
