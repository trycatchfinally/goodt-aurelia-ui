import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'rounded',
      type: Boolean,
      value: false,
      description: 'chip is rounded if <code>true</code>',
    },
  ];
}
