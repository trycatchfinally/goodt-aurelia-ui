// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return (
      `
    <gt-chip repeat.for="chip of chips"
             ${ComponentEvents.DEACTIVATE_READY}.delegate="removeTrigger($event)"  
             rounded.bind="isRounded" 
             class="mar-2" 
             class.bind="'badge-' + chip.color">$` + `{chip.value}</gt-chip>`
    );
  }

  get js() {
    return `
    export class Demo {

      isRounded = false;
      chips = [
        {
          color: false,
          value: 'Um'
        },
        {
          color: 'primary',
          value: 'Dois'
        },
        {
          color: 'success',
          value: 'Tres'
        },
        {
          color: 'warn',
          value: 'Quatro'
        },
        {
          color: 'error',
          value: 'Cinco'
        },
        {
          color: 'misc',
          value: 'Seis'
        }
      ];

      removeTrigger(event) {
        const value = e.detail.value;
        this.chips = this.chips.filter(chip => chip.value != value);
      }
    }`;
  }
}
