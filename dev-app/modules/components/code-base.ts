import { useView } from 'aurelia-framework';

@useView('modules/components/code.html')
export class CodeBase {
  get code() {
    return ``;
  }

  get js() {
    return ``;
  }
}
