import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'message',
      type: String,
      description: 'toast text',
    },
    {
      name: 'duration',
      type: Number,
      value: 2100,
      description: 'duration time in milliseconds to display toast',
    },
  ];
}
