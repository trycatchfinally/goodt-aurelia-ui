import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-toast message.bind="message"></gt-toast>

    <gt-toast message="Event detected" 
              duration="3000"></gt-toast>`;
  }

  get js() {
    return `
    export class Demo {
    
      message = 'Toast text';
     
    }`;
  }
}
