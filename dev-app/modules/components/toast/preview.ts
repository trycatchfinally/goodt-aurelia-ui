import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public message: string = 'Toast text';
  public counter: number = 0;

  increase = () => this.counter++;

  get displayedMessage(): string | boolean {
    return this.counter ? `${this.message} #${this.counter}` : false;
  }
}
