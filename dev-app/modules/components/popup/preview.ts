// @ts-ignore
import { IButtonAction } from 'resources/interfaces';

import { PreviewBase } from '../preview-base';

export class Preview extends PreviewBase {
  public isFixed: boolean = false;
  public isActive: boolean = false;
  public hasCloseIcon: boolean = true;
  public buttons: IButtonAction[];
  public lastAction: string;

  bind() {
    this.buttons = [
      {
        title: 'cancel',
        cssClass: 'btn-ghost',
        action: this.cancelTrigger,
      },
      {
        title: 'save',
        cssClass: 'pull-right btn-primary mar-left-3',
        action: this.saveTrigger,
      },
      {
        title: 'apply',
        cssClass: 'pull-right btn-outline',
        action: this.applyTrigger,
      },
    ];
  }

  close = () => {
    this.isActive = false;
    this.isFixed = false;
    this.hasCloseIcon = true;
  }

  cancelTrigger = () => {
    this.lastAction = 'cancel';
    this.close();
  }

  applyTrigger = () => {
    this.lastAction = 'apply';
  }

  saveTrigger = () => {
    this.lastAction = 'save';
  }
}
