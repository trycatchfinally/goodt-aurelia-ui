// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigProperty } from '../../../types';
import { PropertiesBase } from '../properties-base';

export class Properties extends PropertiesBase {
  items: IConfigProperty[] = [
    {
      name: 'isFixed',
      value: true,
      type: Boolean,
      description: 'fixed position',
    },
    {
      name: 'isCloseable',
      value: true,
      type: Boolean,
      description: `popup can be closed if <code>true</code>, see <code>${ComponentEvents.DEACTIVATE_READY}</code> event for complete removal`,
    },
    {
      name: 'buttons',
      type: Array,
      description:
        'description for buttons with <code>title</code>, <code>action</code>, <code>cssClass</code> properties',
    },
  ];
}
