// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { CodeBase } from '../code-base';

export class Code extends CodeBase {
  get code() {
    return `
    <gt-popup
      is-fixed.bind="isFixed"
      is-closeable.bind="hasCloseIcon"
      buttons.bind="buttons"
      ${ComponentEvents.DEACTIVATE_READY}.delegate="close()"
      if.bind="isActive">
      <h3>Popup title</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
    </gt-popup>`;
  }

  get js() {
    return `
    export class Demo {
    
      isFixed = false;
      isActive = false;
      hasCloseIcon = true;
      buttons;
      lastAction;
    
      bind() {
        this.buttons = [
          {
            title: 'cancel',
            cssClass: 'btn-ghost',
            action: this.cancelTrigger
          },
          {
            title: 'save',
            cssClass: 'pull-right btn-primary mar-left-3',
            action: this.saveTrigger
          },
          {
            title: 'apply',
            cssClass: 'pull-right btn-outline',
            action: this.applyTrigger
          }
        ];
      }
    
      close = () => {
        this.isActive = false;
        this.isFixed = false;
        this.hasCloseIcon = true;
      }
    
      cancelTrigger = () => {
        this.lastAction = 'cancel';
        this.close();
      }
    
      applyTrigger = () => {
        this.lastAction = 'apply';
      }
    
      saveTrigger = () => {
        this.lastAction = 'save';
      }
    }`;
  }
}
