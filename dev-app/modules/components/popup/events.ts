// @ts-ignore
import { ComponentEvents } from 'resources/constants';

import { IConfigEvent } from '../../../types';
import { EventsBase } from '../events-base';

export class Properties extends EventsBase {
  items: IConfigEvent[] = [
    {
      name: ComponentEvents.DEACTIVATE_READY,
      description: `ready for complete removal by parent method, e.g. <code>${ComponentEvents.DEACTIVATE_READY}.delegate="removeTrigger($event)"</code>`,
    },
  ];
}
