import { computedFrom, Container } from 'aurelia-framework';
import { Router } from 'aurelia-router';
// @ts-ignore
import { calendars, numberingSystems } from 'resources/bcp47';
// @ts-ignore
import { IErrorDetails, INameValue } from 'resources/interfaces';

import { locales } from '../../data/locales';
import { PreviewBase } from './preview-base';

export class PickerPreview extends PreviewBase {
  public router: Router;

  public selectedLocale: string;
  public selectedNumeralSystem: string;
  public selectedCalendar: string;

  public errorDetails: IErrorDetails;
  public numberingSystems: INameValue[];
  public locales: INameValue[];
  public calendars: INameValue[];
  public returnedValue: string = '';
  public forceGregory: boolean = false;
  protected showCalendarsControl: boolean = true;

  activate() {
    this.router = Container.instance.get(Router);
  }

  bind() {
    let sorted = new Map([...locales.entries()].sort());

    this.locales = [...sorted.entries()].map((_) => {
      return { name: _[0], value: _[1] };
    });
    this.calendars = calendars.sort().map((_: string) => {
      return { name: _, value: _ };
    });
    this.numberingSystems = numberingSystems.sort().map((_: string) => {
      return { name: _, value: _ };
    });

    this.selectedLocale = locales.entries().next().value.slice(-1)[0];
  }

  updateErrorStatus = (errorDetails: IErrorDetails) => {
    this.errorDetails = errorDetails;
  }

  @computedFrom('selectedLocale', 'selectedCalendar', 'selectedNumeralSystem', 'forceGregory')
  get computedLocale() {
    let tags = ['u'];
    if (this.forceGregory) this.selectedCalendar = 'gregory';
    if (this.selectedCalendar) tags.push('ca', this.selectedCalendar);
    if (this.selectedNumeralSystem) tags.push('nu', this.selectedNumeralSystem);

    if (tags.length > 1) {
      tags.unshift(this.selectedLocale);
      return tags.join('-');
    }
    return this.selectedLocale;
  }
}
