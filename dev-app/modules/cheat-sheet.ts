import { useView } from 'aurelia-framework';

import { IConfigSections } from '../types';
import { KebabToUpperValueConverter } from '../value-converters/kebab-to-upper';
import config from './components/index';

@useView('./anchored-sections-wrapper.html')
export class CheatSheet {
  config: IConfigSections = {
    title: 'Cheat Sheet',
    path: false,
    altPath: '../modules/cheat-sheet/details',
    menu: null,
    code: null,
  };

  currentAnchor: string;

  created() {
    const converter = new KebabToUpperValueConverter();
    const availableComponents: string[] = [...config.keys()].sort();

    this.config.menu = new Map<string, string>();
    availableComponents.forEach((c) => {
      this.config.menu.set(c, converter.toView(c));
    });
  }
}
