import { useView } from 'aurelia-framework';

import { IConfigSections } from '../types';

@useView('./anchored-sections-wrapper.html')
export class GetStarted {
  config: IConfigSections = {
    title: 'Get Started',
    path: 'get-started',
    menu: null,
    code: null,
  };

  currentAnchor: string;

  created() {
    this.config.menu = new Map<string, string>([
      ['install', 'Installation'],
      ['init', 'Plugin initialization'],
      ['styles', 'Styles'],
      ['assets', 'Fonts and Icons'],
    ]);

    this.config.code = {
      install: `npm install goodt-aurelia-ui --save`,
      init: `  
    // main.js
    import { Aurelia } from 'aurelia-framework';

    export function configure(aurelia) {
      aurelia.use
        .standardConfiguration()
        .feature('resources');

        aurelia.use.plugin('goodt-aurelia-ui');

        aurelia.start().then(() => aurelia.setRoot());
    }`,
      styles: {
        prod: `npm i goodt-framework-css --save-dev`,
        prototyping: `
    // main.js
    import 'goodt-framework-css/dist/all.css'`,
        copy: `
    // aurelia.json
    "build": {
      "copyFiles": {
        "node_modules/goodt-framework-css/dist/all.css": "styles"
      }
    }`,
        less: `
    // styles.less
    @import './node_modules/goodt-framework-css/src/_all';
    ...`,
        require: `
    // app.html
    <template>
        <require from="./styles.css"></require>
        ...
    <template>`,
      },
      assets: {
        prod: `npm i @mdi/font --save-dev`,
        copy: `
    // aurelia.json
    "build": {
      "copyFiles": {
        "node_modules/goodt-framework-css/fonts/bold/*": "assets/fonts/bold",
        "node_modules/goodt-framework-css/fonts/regular/*": "assets/fonts/regular",
        "node_modules/@mdi/font/fonts/*": "assets/fonts",
        "node_modules/@mdi/font/css/materialdesignicons.min.css": "assets/css"
      }
    }`,
        markup: `
    <style type="text/css">
      @import '/assets/fonts/bold/pt-root-ui-bold.css';
      @import '/assets/fonts/regular/pt-root-ui-regular.css';
      @import '/assets/css/materialdesignicons.min.css';
    </style>`,
      },
    };
  }
}
