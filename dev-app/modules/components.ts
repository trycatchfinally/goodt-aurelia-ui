import { NavigationInstruction, RouteConfig, Router } from 'aurelia-router';
// @ts-ignore
import { components } from 'resources/constants';

// @ts-ignore
import * as info from '../config/package.json';
import environment from '../environment';
import config from './components/index';

export class Components {
  public components: string[] = components;
  public availableComponents: string[] = [...config.keys()].sort();
  public router: Router;
  public tracking: boolean = (environment as any)?.tracking ?? false;

  activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    this.router = navigationInstruction.router;
  }

  get plannedComponents() {
    return this.components.filter((c) => !this.availableComponents.includes(c)).sort();
  }

  get version() {
    return info?.version ?? false;
  }
}
