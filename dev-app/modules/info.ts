// @ts-ignore
import * as info from '../config/package.json';

export class Info {
  summary: string = JSON.stringify(info, null, 2);
}
