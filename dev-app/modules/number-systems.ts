import { bindable } from 'aurelia-framework';
// @ts-ignore
import { numberingSystems } from 'resources/bcp47';
// @ts-ignore
import { summarizeNumberingSystem } from 'resources/components/base/helpers';

interface INumberingSystemDetails {
  name: string;
  values: string[];
}

export class NumberSystems {
  @bindable numberingSystems: INumberingSystemDetails[];
  @bindable numeralNext: INumberingSystemDetails[];

  bind() {
    this.numberingSystems = numberingSystems.sort().map((name: string) => {
      const summary = summarizeNumberingSystem(name);
      return {
        name: name,
        values: summary,
      };
    });
  }
}
