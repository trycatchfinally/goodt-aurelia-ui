import { bindable, Controller } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
import { NavigationInstruction, RouteConfig, Router } from 'aurelia-router';
// @ts-ignore
import { components } from 'resources/constants';

import { IConfig } from '../types';
import { CamelToKebabValueConverter } from '../value-converters/camel-to-kebab';
import config from './components/index';

export const EXTERNAL_BASE_URL = 'https://goodt-css.netlify.com/';

export class ComponentDetails {
  @bindable public previewController: Controller;

  public component: string = '';
  public router: Router;
  public config: IConfig;
  public availableComponents: string[] = [...config.keys()].sort();
  public currentMode: string;
  public initialized: boolean;

  private storedController: Controller;
  private vc = new CamelToKebabValueConverter();

  attached() {
    this.initialized = true;
    PLATFORM.global.scrollTo(0, 0);
  }

  activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    this.router = navigationInstruction.router;

    if ((components as string[]).includes(params?.component)) {
      this.component = params.component;

      if (config.has(this.component)) {
        this.currentMode = '';
        this.storedController = null;
        this.config = config.get(this.component);
        routeConfig.navModel.setTitle(`gt-${this.component}`);

        setTimeout(() => (this.initialized = true), 200);
      } else {
        this.onFallback();
      }
    } else {
      this.onFallback();
    }
  }

  canDeactivate() {
    this.initialized = false;
  }

  previewControllerChanged(newValue, oldValue) {
    if (newValue && !this.storedController) this.storedController = newValue;
  }

  private onFallback() {
    this.router.navigateToRoute('components');
  }

  get previewViewModel() {
    return (this?.storedController as any)?.viewModel;
  }

  get hasSlots() {
    return typeof this.previewViewModel?.validSlot !== 'undefined';
  }

  get externalUrl(): string | boolean {
    return this.config?.url ? `${EXTERNAL_BASE_URL}${this.config.url}` : false;
  }

  get validSlot(): string | boolean {
    if (this.config?.validSlot) return this.config.validSlot;
    return this.previewViewModel?.validSlot?.$resource ?? false;
  }

  get validSlotComponentName(): string {
    if (!this.validSlot) return;
    return this.vc.toView(this.validSlot as string).replace('gt-', '');
  }

  get modes() {
    let modes = ['preview', 'code'].filter((v) => this.config[v]);
    this.currentMode || (this.currentMode = modes[0]);
    return modes;
  }

  get currentModePath() {
    return `./components/${this.component}/${this.currentMode}`;
  }

  get isPreviewComponentAvailable() {
    return this.initialized && Boolean(this.storedController);
  }
}
