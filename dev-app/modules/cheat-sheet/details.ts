import { Container } from 'aurelia-framework';
import { Router } from 'aurelia-router';

import config from '../components/index';

export class Details {
  public settings: any;
  public component: string;
  public router: Router;

  activate(model: string) {
    this.router = Container.instance.get(Router);
    this.component = model;
    this.settings = config.get(this.component);
  }
}
