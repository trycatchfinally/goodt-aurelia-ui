// see https://en.wikipedia.org/wiki/IETF_language_tag
export const locales: Map<string, string> = new Map([
  ['English', 'en-US'],
  ['French', 'fr'],
  ['German', 'de'],
  ['Russian', 'ru'],
  ['Arabic', 'ar-SA'],
  ['Farsi', 'fa-IR'],
  ['Hebrew', 'he-IL'],
  ['Chinese', 'zh'],
  ['Korean', 'ko'],
  ['Japanese', 'ja-JP'],
  ['Hindi', 'hi'],
  ['Turkish', 'tr'],
]);
