export interface IProduct {
  name: string;
  value: number;
}

export const products: IProduct[] = [
  { value: 0, name: 'Motherboard' },
  { value: 1, name: 'CPU' },
  { value: 2, name: 'Memory' },
  { value: 3, name: 'Drives' },
  { value: 4, name: 'Display' },
  { value: 5, name: 'Keyboard' },
];
