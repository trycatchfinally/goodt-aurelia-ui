import { bindable } from 'aurelia-framework';

export class ExternalLinkCustomElement {
  @bindable url: string;
  @bindable icon: string = 'mdi-open-in-new';
  @bindable title: string = 'the link will open in a new window';
}
