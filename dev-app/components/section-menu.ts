import { autoinject } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
// @ts-ignore
import { findParentElement } from 'resources/components/base/helpers';

@autoinject
export class SectionMenuCustomElement {
  resizeTimer: any;
  resizeEventHandler: any;

  contextEl: HTMLElement;
  originalEl: HTMLElement;
  menuEl: HTMLElement;

  constructor(readonly element: Element) {
    this.resizeEventHandler = this.resized.bind(this);
  }

  attached() {
    this.contextEl = document.getElementById('context-menu');
    this.originalEl = this.element.querySelector('.menu');
    this.menuEl = this.originalEl.querySelector('ul');

    this.resized();
    PLATFORM.global.addEventListener('resize', this.resizeEventHandler);
  }

  detached() {
    PLATFORM.global.removeEventListener('resize', this.resizeEventHandler);
  }

  resized() {
    clearTimeout(this.resizeTimer);

    this.resizeTimer = setTimeout(() => {
      if (this.originalEl.getBoundingClientRect().width) {
        this.originalEl.append(this.menuEl);

        const offcanvas = findParentElement(this.contextEl, 'gt-off-canvas');
        if (offcanvas) offcanvas.au.controller.viewModel.active = false;
      } else {
        this.contextEl.append(this.menuEl);
      }
    }, 150);
  }
}
