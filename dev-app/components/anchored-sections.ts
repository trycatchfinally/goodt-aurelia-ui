import { autoinject, bindable } from 'aurelia-framework';

@autoinject
export class AnchoredSectionsCustomElement {
  @bindable menu: Map<string, any> = new Map<string, any>();
  @bindable title: string;
  @bindable path: string;
  @bindable altPath: string;
  @bindable code: any;
  @bindable currentAnchor: string;

  private scrollHandler: any;

  constructor(readonly element: Element) {}

  attached() {
    this.currentAnchor = this.anchors?.[0];
    const vm = this;
    vm.scrollHandler = () => vm.syncScroll();
    document.addEventListener('scroll', vm.scrollHandler);
  }

  detached() {
    const vm = this;
    document.removeEventListener('scroll', vm.scrollHandler);
    this.scrollHandler = undefined;
  }

  syncScroll() {
    const anchor = [...(document.querySelectorAll('a[name]') as any)].find((e) => e.getBoundingClientRect().top >= 0);
    const currentAnchor = anchor?.getAttribute('name');

    if (currentAnchor !== this.currentAnchor) {
      this.currentAnchor = currentAnchor;
    }
  }

  scroll(id: string): void {
    const el = document.querySelector(`[name="${id}"]`);
    if (el)
      el.scrollIntoView({
        block: 'start',
        inline: 'nearest',
        behavior: 'smooth',
      });
  }

  get anchors(): string[] {
    return [...this.menu.keys()];
  }
}
