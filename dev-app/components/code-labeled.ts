import { bindable } from 'aurelia-framework';

export class CodeLabeledCustomElement {
  @bindable label: string;
  @bindable lang: string;
  @bindable code: string;
  @bindable offsetCssClass: string = 'mar-top-5';

  get cssClasses() {
    return this.offsetCssClass;
  }
}
