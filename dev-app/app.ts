import { EventAggregator } from 'aurelia-event-aggregator';
import { PLATFORM } from 'aurelia-pal';
import { Router, RouterConfiguration, RouterEvent } from 'aurelia-router';

import routes from './routes';

export class App {
  static inject = [EventAggregator];

  offCanvasActive: boolean = false;
  router: Router;

  constructor(ea: EventAggregator) {
    ea.subscribe(RouterEvent.Processing, () => (this.offCanvasActive = false));
    ea.subscribe(RouterEvent.Complete, () => this.toTop());
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    this.router = router;
    config.title = 'Goodt UI';
    config.map(routes);
    config.mapUnknownRoutes('get-started');
  }

  toTop() {
    PLATFORM.global.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
}
