export class TagNameValueConverter {
  toView(value: string): string {
    return `gt-${value}`;
  }
}
