export class TrimCodeValueConverter {
  toView(value: string): string {
    const spaces = Array(4).fill(' ').join('');
    const tabs = Array(2).fill('\t').join('');
    const separator = '\n';

    return value
      .split(separator)
      .filter((s, i) => i !== 0)
      .map((str) => str.replace(spaces, ''))
      .map((str) => str.replace(tabs, ''))
      .join(separator);
  }
}
