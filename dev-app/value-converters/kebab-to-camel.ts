export class KebabToCamelValueConverter {
  toView(value: string): string {
    return value.replace(/-([a-z])/g, (x, up) => up.toUpperCase());
  }
}
