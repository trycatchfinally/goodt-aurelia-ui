export class KebabToUpperValueConverter {
  toView(value: string): string {
    return value
      .split('-')
      .map((v) => v.charAt(0).toUpperCase() + v.slice(1))
      .join(' ');
  }
}
