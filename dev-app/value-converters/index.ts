import { CamelToKebabValueConverter } from './camel-to-kebab';
import { KebabToCamelValueConverter } from './kebab-to-camel';
import { KebabToUpperValueConverter } from './kebab-to-upper';
import { TagNameValueConverter } from './tag-name';
import { TrimCodeValueConverter } from './trim-code';

export default [
  KebabToCamelValueConverter,
  KebabToUpperValueConverter,
  CamelToKebabValueConverter,
  TrimCodeValueConverter,
  TagNameValueConverter,
];
