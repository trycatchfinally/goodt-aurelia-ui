import { RouteConfig } from 'aurelia-router';

// todo: custom attributes long-press, au-bind, swipe-detect
const routes: RouteConfig[] = [
  {
    route: ['', 'get-started'],
    name: 'getStarted',
    moduleId: 'modules/get-started',
    title: 'Get Started',
    nav: 0,
  },
  {
    route: 'components',
    name: 'components',
    moduleId: 'modules/components',
    title: 'Components',
    nav: 1,
  },
  {
    route: 'components/:component',
    name: 'componentDetails',
    moduleId: 'modules/component-details',
    title: 'Component Details',
    nav: false,
  },
  {
    route: 'cheat-sheet',
    name: 'cheatSheet',
    moduleId: 'modules/cheat-sheet',
    title: 'Cheat Sheet',
    nav: 2,
  },
  {
    route: 'package.json',
    name: 'info',
    moduleId: 'modules/info',
    title: 'Build Info',
    nav: false,
  },
  {
    route: 'numbers',
    name: 'numbers',
    moduleId: 'modules/number-systems',
    title: 'Numbering systems for picker components',
    nav: false,
  },
];

export default routes;
