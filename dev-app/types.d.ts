export interface IConfig {
  preview: boolean;
  code: boolean;
  description?: boolean;
  url?: string;
  properties?: boolean;
  events?: boolean;
  validSlot?: any;
  incomplete?: boolean;
  tbd?: boolean;
}

export interface IConfigProperty {
  name: string;
  value?: any;
  type: any;
  description?: any;
  required?: boolean;
}

export interface IConfigEvent {
  name: string;
  description?: any;
}

export interface IConfigSections {
  title: string;
  path: string | boolean;
  altPath?: string;
  menu: Map<string, string>;
  code: any;
}
