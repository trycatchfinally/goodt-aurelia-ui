import { Aurelia } from 'aurelia-framework';
// import 'core-js/stable'
import { PLATFORM } from 'aurelia-pal';

import environment from './environment';

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .globalResources([
      PLATFORM.moduleName('./value-converters/kebab-to-camel'),
      PLATFORM.moduleName('./value-converters/kebab-to-upper'),
      PLATFORM.moduleName('./value-converters/camel-to-kebab'),
      PLATFORM.moduleName('./value-converters/trim-code'),
      PLATFORM.moduleName('./value-converters/tag-name'),
    ])
    .plugin('aurelia-syntax-highlighter')
    // load the plugin ../src
    // The "resources" is mapped to "../src" in aurelia.json "paths"
    .feature('resources');

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  const tracking = (environment as any).tracking ?? false;

  aurelia.use.plugin('aurelia-google-analytics', (config: any) => {
    config.init('UA-158689440-1');
    config.attach({
      logging: {
        // Set to `true` to have some log messages appear in the browser console.
        enabled: false,
      },
      pageTracking: {
        // Set to `false` to disable in non-production environments.
        enabled: tracking,
        // Configure fragments/routes/route names to ignore page tracking for
        /*
        ignore: {
          fragments: [], // Ignore a route fragment, login fragment for example: ['/login']
          routes: [], // Ignore a route, login route for example: ['login']
          routeNames: [] // Ignore a route name, login route name for example: ['login-route']
        },
        // Optional. By default it gets the title from payload.instruction.config.title.
        getTitle: (payload) => {
          // For example, if you want to retrieve the tile from the document instead override with the following.
          return document.title;
        },
        // Optional. By default it gets the URL fragment from payload.instruction.fragment.
        getUrl: (payload) => {
          // For example, if you want to get full URL each time override with the following.
          return window.location.href;
        }
        */
      },
      clickTracking: {
        // Set to `false` to disable in non-production environments.
        enabled: tracking,
        // Optional. By default it tracks clicks on anchors and buttons.
        /*
        filter: (element) => {
          // For example, if you want to also track clicks on span elements override with the following.
          return element instanceof HTMLElement &&
            (element.nodeName.toLowerCase() === 'a' ||
              element.nodeName.toLowerCase() === 'button' ||
              element.nodeName.toLowerCase() === 'span');
        }
        */
      },
      exceptionTracking: {
        // Set to `false` to disable in non-production environments.
        enabled: tracking,
      },
    });
  });

  aurelia.start().then(() => aurelia.setRoot());
}
