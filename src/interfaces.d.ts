export interface INameValue {
  name: string | number;
  value: any;
  disabled?: boolean;
}

export interface IButtonAction {
  title: string;
  action: any;
  cssClass?: string;
}

export interface IErrorDetails {
  status: boolean;
  code?: number;
  description?: string;
}

export interface ICalendarEmpty {
  empty: boolean;
}

export interface ICalendarDay extends INameValue {
  empty: boolean;
}

export interface ICalendarWeek {
  days: ICalendarDay[] | ICalendarEmpty[];
}

export interface ICalendarMonth {
  title: string;
  year: string;
  weeks: ICalendarWeek[];
  label: string;
}

export interface ICalendar {
  months: ICalendarMonth[];
  weekdays: string[];
}

export interface ICalendarParams {
  isScrolling: boolean;
  initialized: boolean;
  date: Date;
  estimated?: Date;
  calendar?: {
    index: number;
  };
  years?: {
    elHeight: number;
  };
}
