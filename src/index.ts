import './styles.css';

import { FrameworkConfiguration } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

// import {components} from './constants'

export function configure(config: FrameworkConfiguration) {
  /*
  // Commented code below is not working due to Webpack issue
  // https://github.com/aurelia/webpack-plugin/issues/107#issuecomment-293878340
  
  const resources = [];
  components.forEach(component => resources.push(PLATFORM.moduleName(`./components/gt-${component}`)));
  config.globalResources(resources);
  */

  config.globalResources([
    PLATFORM.moduleName('./components/gt-alert'),
    PLATFORM.moduleName('./components/gt-avatar'),
    PLATFORM.moduleName('./components/gt-checkbox'),
    PLATFORM.moduleName('./components/gt-chip'),
    PLATFORM.moduleName('./components/gt-date-picker'),
    PLATFORM.moduleName('./components/gt-details'),
    PLATFORM.moduleName('./components/gt-dropdown'),
    PLATFORM.moduleName('./components/gt-grid'),
    PLATFORM.moduleName('./components/gt-grid-column'),
    PLATFORM.moduleName('./components/gt-menu'),
    PLATFORM.moduleName('./components/gt-number-field'),
    PLATFORM.moduleName('./components/gt-off-canvas'),
    PLATFORM.moduleName('./components/gt-popup'),
    PLATFORM.moduleName('./components/gt-preloader'),
    PLATFORM.moduleName('./components/gt-radio'),
    PLATFORM.moduleName('./components/gt-select'),
    PLATFORM.moduleName('./components/gt-shim'),
    PLATFORM.moduleName('./components/gt-switch'),
    PLATFORM.moduleName('./components/gt-tabs'),
    PLATFORM.moduleName('./components/gt-time-picker'),
    PLATFORM.moduleName('./components/gt-toast'),
    PLATFORM.moduleName('./components/gt-tree'),
    PLATFORM.moduleName('./attributes/au-bind'),
    PLATFORM.moduleName('./attributes/long-press'),
    PLATFORM.moduleName('./attributes/swipe-detect'),
  ]);
}
