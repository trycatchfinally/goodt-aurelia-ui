import { bindable } from 'aurelia-framework';

import { CoreComponent } from './base/core-component';

export class GtNumberField extends CoreComponent {
  // input
  @bindable public value: any;
  @bindable public min: number;
  @bindable public max: number;
  @bindable public step: number = 1;
  @bindable public inputClass: string;

  // controls
  @bindable public skipSteps = 10;
  @bindable public hasControls: boolean = false;
  @bindable public ctrlClass: string;

  // rebindable input properties
  @bindable public disabled: boolean;
  @bindable public readonly: boolean;
  @bindable public required: boolean;

  private inputElement: HTMLInputElement;

  decrease(step = 1) {
    this.inputElement.stepDown(step);
    this.verify(+this.inputElement.value);
  }

  increase(step = 1) {
    this.inputElement.stepUp(step);
    this.verify(+this.inputElement.value);
  }

  update(event: Event) {
    this.verify(+(event.target as HTMLInputElement).value);
  }

  private verify(value: number) {
    if (this.min && this.min > value) value = this.min;
    if (this.max && this.max < value) value = this.max;
    this.value = `${value}`;
  }

  protected get cssClasses(): string {
    const map = new Map([
      ['disabled', 'disabled'],
      ['hasControls', 'text-center'],
    ]);
    let classes = [...map.keys()].filter((p) => this[p]).map((p) => map.get(p));
    if (this.inputClass) classes.push(this.inputClass);
    return classes.join(' ');
  }

  protected get cssDecrease(): string {
    return this.ctrlCssClasses(this.min);
  }

  protected get cssIncrease(): string {
    return this.ctrlCssClasses(this.max);
  }

  private ctrlCssClasses(attr: number): string {
    let classes = [];
    if (this.ctrlClass) classes.push(this.ctrlClass);
    classes.push(attr == this.value || this.disabled || this.readonly ? 'events-none text-muted' : 'cursor-pointer');
    return classes.join(' ');
  }
}
