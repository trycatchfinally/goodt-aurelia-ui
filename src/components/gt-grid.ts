import { autoinject, bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';
import { GtGridColumn } from './gt-grid-column';

@autoinject
export class GtGrid extends SlotComponent {
  @bindable public collapse: boolean = false;

  created(owningView: any, myView: any) {
    this.validSlot = GtGridColumn;
    super.created(owningView, myView);
  }
}
