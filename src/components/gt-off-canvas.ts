import { bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

export class GtOffCanvas extends SlotComponent {
  @bindable public active: boolean = false;
  @bindable public isRight: boolean = false;

  protected get cssClasses(): string {
    const map = new Map([
      ['active', 'active'],
      ['isRight', 'offcanvas-right'],
    ]);
    return [...map.keys()]
      .filter((p) => this[p])
      .map((p) => map.get(p))
      .join(' ');
  }
}
