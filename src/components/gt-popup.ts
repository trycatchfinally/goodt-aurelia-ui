import { bindable } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { IButtonAction } from '../interfaces';
import { SlotComponent } from './base/slot-component';

export class GtPopup extends SlotComponent {
  @bindable isFixed: boolean = true;
  @bindable isCloseable: boolean = true;
  @bindable buttons: IButtonAction[] = [];

  close = () => {
    this.emitEvent(ComponentEvents.DEACTIVATE_READY);
  };
}
