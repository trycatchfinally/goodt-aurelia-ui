import { bindable } from 'aurelia-framework';

import { CoreComponent } from './base/core-component';

export class GtPreloader extends CoreComponent {
  @bindable inverse: boolean = false;
  @bindable size: number;
  @bindable layoutSize: number;
  @bindable color: string;

  private static SIZE: number = 7;
  private static LAYOUT_SIZE: number = 5;

  sizeChanged(newValue: any, oldValue: any) {
    const value = newValue | 0;
    if (GtPreloader.checkSizeRange(GtPreloader.SIZE, value)) {
      this.layoutSize = 0;
      this.size = value;
    } else {
      this.size = 0;
    }
  }

  layoutSizeChanged(newValue, oldValue) {
    const value = newValue | 0;
    if (GtPreloader.checkSizeRange(GtPreloader.LAYOUT_SIZE, value)) {
      this.size = 0;
      this.layoutSize = value;
    } else {
      this.layoutSize = 0;
    }
  }

  protected get cssClasses(): string {
    const classes = [];

    if (this.inverse) classes.push('preloader-inverse');
    if (this.size) classes.push(`pad-${this.size}`);
    if (this.layoutSize) classes.push(`pad-l${this.layoutSize}`);
    if (this.color?.length && !this.inverse) classes.push(`color-${this.color}`);

    return classes.join(' ');
  }

  private static checkSizeRange(max: number, value: number): boolean {
    return [...Array(max).keys()].includes(value - 1);
  }
}
