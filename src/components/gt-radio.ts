import { bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

export class GtRadio extends SlotComponent {
  @bindable isSmall: boolean = false;

  bind() {
    this.checkRequired(['name']);
  }

  get labelClass(): string {
    let classes = [];
    const input = this.element.querySelector('input');

    if (this.isSmall) classes.push('radio-small');
    if (input.disabled) classes.push('disabled');
    classes.push(input.checked ? 'events-none' : 'cursor-pointer');

    return classes.join(' ');
  }
}
