import { bindable } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { CoreComponent } from './base/core-component';

// todo: add loading status?
export class GtDropdown extends CoreComponent {
  @bindable items: string[] = [];
  @bindable active: number;
  @bindable disabled: number[] = [];

  protected selectTrigger(index: number) {
    if (this.disabled.includes(index) || index === this.active) return;

    this.active = index;
    this.emitEvent(ComponentEvents.ITEM_SELECT, {
      detail: {
        index,
      },
    });
  }
}
