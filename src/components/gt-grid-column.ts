import { autoinject, bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

@autoinject
export class GtGridColumn extends SlotComponent {
  static $resource = 'gt-grid-column';

  @bindable public auto: boolean = false;
  @bindable public mode: number = 12;
  @bindable public size: number = 0;

  private modes: Set<number> = new Set([0, 12, 24]);
  private thirds: number[] = [33, 66];
  private isUpdating: boolean = false;

  autoChanged(newValue: any, oldValue: any) {
    if (this.isUpdating) return;
    this.isUpdating = true;
    if (newValue) {
      this.mode = 0;
      this.size = 0;
    }
    this.normalizeClassList();
  }

  modeChanged(newValue: number, oldValue: any) {
    oldValue = oldValue | 0;
    newValue = newValue | 0;
    if (!this.modes.has(newValue) || oldValue === true || this.isUpdating) return;
    this.isUpdating = true;

    const modes = [...this.modes];
    const toAdjust = () => {
      this.size = 0;
      this.mode = 0;
    };
    const toPercentage = () => {
      if (this.size) {
        const calculatedWidth = Math.floor(100 / (oldValue / this.size));
        if (this.thirds.includes(calculatedWidth) || [0, 5].includes(calculatedWidth % 10)) {
          this.size = calculatedWidth;
        } else {
          toAdjust();
        }
      } else {
        toAdjust();
      }
    };

    switch (oldValue) {
      case modes[1]:
        switch (newValue) {
          case modes[0]:
            toPercentage();
            break;
          case modes[2]:
            this.size = this.size * 2;
            break;
        }
        break;
      case modes[2]:
        switch (newValue) {
          case modes[0]:
            toPercentage();
            break;
          case modes[1]:
            if (this.size % 2 === 0) {
              this.size = this.size / 2;
            } else {
              toAdjust();
            }
            break;
        }
        break;
      case modes[0]:
      default:
        switch (newValue) {
          case modes[1]:
          case modes[2]:
            if (this.auto) {
              toAdjust();
            } else {
              this.size = Math.floor((newValue * this.size) / 100);
            }
            break;
        }
        break;
    }
    this.normalizeClassList();
  }

  sizeChanged(newValue: number, oldValue: any) {
    if (this.isUpdating) return;
    this.isUpdating = true;

    newValue = newValue | 0;
    let valid: boolean;
    const modes = [...this.modes];

    switch (this.mode) {
      case modes[1]:
        valid = Array(modes[1])
          .fill(1)
          .map((x, y) => x + y)
          .includes(newValue);
        break;
      case modes[2]:
        valid = Array(modes[2])
          .fill(1)
          .map((x, y) => x + y)
          .includes(newValue);
        break;
      default:
        valid = [
          ...Array(20)
            .fill(5)
            .map((x, y) => x * (y + 1)),
          ...this.thirds,
        ].includes(newValue);
        break;
    }

    if (!valid) this.size = 0;
    this.normalizeClassList();
  }

  private normalizeClassList(): void {
    if (!this.isUpdating) return;
    const modes = [...this.modes];
    this.classList.value
      .split(' ')
      .filter((c) => c.indexOf('col-') === 0)
      .forEach((c) => this.classList.remove(c));
    this.classList.add(
      this.auto
        ? 'col-auto'
        : this.size
        ? !this.mode
          ? `col-${this.size}`
          : `col-${this.size}-${this.mode === modes[2] ? modes[2] : modes[1]}`
        : 'col',
    );
    this.isUpdating = false;
  }
}
