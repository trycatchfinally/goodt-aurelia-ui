import { bindable } from 'aurelia-framework';

import { CoreComponent } from './base/core-component';

export class GtSwitch extends CoreComponent {
  @bindable model: any;
  @bindable active: boolean = false;
  @bindable isSmall: boolean = false;
  @bindable state: string;

  private states: Set<string> = new Set(['hover', 'focus', 'disabled', 'valid', 'invalid']);

  stateChanged(newValue: string, oldValue: any) {
    if (!this.states.has(newValue)) this.state = undefined;
  }

  protected get cssClasses(): string {
    const classes = [];

    if (this.isSmall) classes.push('switch-small');
    if (this.state) classes.push(this.state);

    return classes.join(' ');
  }

  get disabled(): boolean {
    return this.state === 'disabled';
  }
}
