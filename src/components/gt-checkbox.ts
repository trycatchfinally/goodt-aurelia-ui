import { bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

export class GtCheckbox extends SlotComponent {
  @bindable isSmall: boolean = false;
  @bindable isPartial: boolean = false;

  bind() {
    this.checkRequired(['name']);
  }

  get labelClass(): string {
    let classes = ['cursor-pointer'];
    const input = this.element.querySelector('input');

    if (this.isSmall) classes.push('checkbox-small');
    if (input.disabled) classes.push('disabled');

    return classes.join(' ');
  }

  get isPartially(): boolean {
    return this.isPartial && (this.element.querySelector('input') as HTMLInputElement).checked;
  }
}
