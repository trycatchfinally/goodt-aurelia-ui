import { bindable, computedFrom } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

import { numberingSystems } from '../bcp47';
import { ComponentEvents, ErrorsCodes, smoothScrollHeight } from '../constants';
import { IErrorDetails, INameValue } from '../interfaces';
import { convertFromNumberingSystem } from './base/helpers';
import { PickerInput } from './base/picker-input';

export class GtTimePicker extends PickerInput {
  @bindable step: number = 60;
  @bindable min: string = '00:00';
  @bindable max: string = '23:59';
  @bindable showSeconds: boolean = false;
  @bindable presetsOnly: boolean = false;
  @bindable hasCurrentTimeControl: boolean = false;
  @bindable excluded: string[] = [];
  @bindable locale: string;
  @bindable value: string | Date;

  private dayPeriods: string[] = [];
  private presets: INameValue[] = [];
  private lastKnownDateTimeFormatParts: Intl.DateTimeFormatPart[] = [];
  private lastKnownErrorCode: ErrorsCodes;
  private initialDate: Date;

  private numerals = new Set<string>([
    'twelve',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
  ]);

  bind() {
    super.bind();

    const today = new Date();
    this.initialDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);

    if (!this.locale)
      this.locale = PLATFORM.global.navigator.language || (PLATFORM.global.navigator as any)?.userLanguage;
    if (this.value) {
      const timestamp: number = Date.parse(this.value?.toString());
      if (isNaN(timestamp) === false) {
        let boundDate = new Date(timestamp);
        this.date = this.normalizeDate(boundDate.getHours(), boundDate.getMinutes(), boundDate.getSeconds());
      }
    }
    this.innerFormatter = new Intl.DateTimeFormat('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false,
    });
    this.init();
  }

  valueChanged(newValue: any, oldValue: any) {
    this.updateErrorStatus(false);
    this.updateDate(newValue instanceof Date ? newValue : this.valueToDate(newValue));
  }

  localeChanged(newValue: string, oldValue: string) {
    this.init();
  }

  showSecondsChanged(newValue: boolean, oldValue: boolean) {
    this.init();
  }

  stepChanged(newValue: number, oldValue: number) {
    this.init();
  }

  minChanged(newValue: string, oldValue: string) {
    this.init();
  }

  maxChanged(newValue: string, oldValue: string) {
    this.init();
  }

  excludedChanged(newValue: string[], oldValue: string[]) {
    this.init();
  }

  setCurrentTime = (): void => {
    this.updateErrorStatus(false);
    const now = new Date();
    this.updateDate(this.normalizeDate(now.getHours(), now.getMinutes(), now.getSeconds()));
  };

  onFocus(e: FocusEvent): void {
    this.expanded = true;
  }

  onChange(e: Event): void {
    e.preventDefault();
    this.updateDate(this.valueToDate((e.target as HTMLInputElement).value));
  }

  private proceedEmit = () => {
    const hasValue = this.value && (this.value as string).trim().length;
    const parts = hasValue ? this.valueFormatter.formatToParts(this.date) : [];

    if (JSON.stringify(parts) !== JSON.stringify(this.lastKnownDateTimeFormatParts)) {
      this.emit(parts);
    }
  };

  private emit = (data: any): void => {
    this.lastKnownDateTimeFormatParts = data;
    this.emitEvent(ComponentEvents.VALUE_CHANGED, {
      detail: {
        value: data,
      },
    });
  };

  selectPreset(e: MouseEvent, preset: INameValue): boolean {
    this.updateErrorStatus(false);
    this.updateDate(preset.value);
    this.expanded = false;

    return true;
  }

  private init(): void {
    this.initFormatter();
    this.updateErrorStatus(false);
    this.resolvedOptions = new Intl.DateTimeFormat(this.locale).resolvedOptions();

    if (this.min && this.max && this.min >= this.max) {
      this.updateErrorStatus(true, ErrorsCodes.MIN_MAX);
    }

    // placeholder
    this.placeholder = this.dateToValue(this.normalizeDate(12));

    // presets
    this.presets = [];
    let presets: INameValue[] = [];

    const min: number[] = this.valueToParts(this.min);
    const max: number[] = this.valueToParts(this.max);

    const minPreset = this.normalizeDate(min[0], min[1]);
    const maxPreset = this.normalizeDate(max[0], max[1]);
    let currentPreset = new Date(minPreset.getTime());

    while (currentPreset <= maxPreset) {
      let value = this.dateToValue(currentPreset);
      presets.push({
        name: value,
        value: currentPreset,
        disabled: false,
      });
      currentPreset = new Date(currentPreset.getTime() + this.step * 60 * 1000);
    }

    this.excluded.forEach((v) => {
      const parts = this.valueToParts(v);
      const value = this.dateToValue(this.normalizeDate(parts[0], parts[1]));
      const targetPreset = presets.find((p) => p.name === value);
      if (targetPreset) targetPreset.disabled = true;
    });

    this.presets = presets;

    // dayPeriods
    this.dayPeriods = [this.normalizeDate(0), this.normalizeDate(12)]
      .map(
        (p) =>
          (this.valueFormatter.formatToParts(p) as Intl.DateTimeFormatPart[]).find((t) => t.type === 'dayPeriod')
            ?.value,
      )
      .filter((p) => p);

    // finalize
    this.updateDate(this.date, false);
  }

  private normalizeDate = (hours: number, minutes: number = 0, seconds: number = 0): Date => {
    let date = new Date(this.initialDate.getTime());
    const presetOptions = {
      setHours: hours,
      setMinutes: minutes,
      setSeconds: seconds,
      setMilliseconds: 0,
    };

    Object.keys(presetOptions).map((m) => {
      date = new Date(date[m](presetOptions[m]));
    });

    return date;
  };

  private initFormatter() {
    const seconds = this.showSeconds ? { second: 'numeric' } : {};
    const options = {
      ...{
        hour: 'numeric',
        minute: 'numeric',
      },
      ...seconds,
    };
    this.valueFormatter = new Intl.DateTimeFormat([this.locale, this.fallbackLocale], options);
  }

  private dateToValue(date: Date): string {
    return isNaN(date.getTime()) ? '' : date && this.valueFormatter ? this.valueFormatter.format(date) : '';
  }

  private valueToDate(value: string): Date | null {
    if (!value) return null;

    value = value.trim();

    const halfDayHours = 12;
    const dayPeriod = this.dayPeriods.find((dp) => value.indexOf(dp) !== -1);
    const parts = this.valueToParts(value, true);

    let hours = parts?.[0];
    let minutes = parts?.[1];
    let seconds = parts?.[2] ?? 0;

    if (dayPeriod) {
      switch (dayPeriod) {
        case this.dayPeriods?.[0]:
          if (hours >= halfDayHours) {
            hours -= halfDayHours;
          }
          break;
        case this.dayPeriods?.[1]:
          if (!(hours >= halfDayHours)) {
            hours += halfDayHours;
          }
          break;
      }
    }

    try {
      // invalid time value
      this.hasError ||
        this.updateErrorStatus(
          hours < 0 || hours > 23 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59,
          ErrorsCodes.INVALID_VALUE,
        );

      let serialized = this.innerFormatter.format(this.normalizeDate(hours, minutes, seconds));
      const zeroHourPrefix = '24:';
      if (!this.showSeconds)
        serialized = serialized
          .split(':')
          .filter((v: string, i: number) => i != 2)
          .join(':');
      if (serialized.indexOf(zeroHourPrefix) === 0) serialized = serialized.replace(zeroHourPrefix, '00:');

      // min/max values
      this.hasError || this.updateErrorStatus(this.min >= this.max, ErrorsCodes.MIN_MAX);

      // min/max, out of range
      this.hasError ||
        this.updateErrorStatus(!(this.min <= serialized && this.max >= serialized), ErrorsCodes.OUT_OF_RANGE);

      // excludes
      this.hasError ||
        this.updateErrorStatus(
          this.excluded.some((v) => v === serialized),
          ErrorsCodes.NOT_EXCLUDED,
        );

      // try to format
      if (this.hasError) this.value = this.valueFormatter.format(this.normalizeDate(hours, minutes, seconds));
    } catch (e) {
      this.updateErrorStatus(true, ErrorsCodes.INVALID_VALUE);
    }

    return this.hasError ? null : this.normalizeDate(hours, minutes, seconds);
  }

  private valueToParts = (value: string, removeDayPeriod: boolean = false): number[] => {
    let parts = value?.split(':');

    if (parts.length === 1) {
      this.dayPeriods.forEach((dp) => (value = value.replace(dp, '')?.trim()));
      if (value.length % 2 === 1) value = `0${value}`;
      parts = value.match(/.{1,2}/g);
    }

    return parts?.map((p: any) => {
      if (removeDayPeriod) {
        this.dayPeriods.forEach((dp) => (p = p.replace(dp, '')?.trim()));
      }
      p = convertFromNumberingSystem(p, this.resolvedOptions.numberingSystem);
      return parseInt(p);
    });
  };

  private updateDate = (date: Date, emit: boolean = true): void => {
    if (!date) {
      emit && this.emit([]);
      return;
    }

    if (this.date?.getTime() != date.getTime()) this.date = date;
    this.value = this.dateToValue(this.date);

    if (emit) {
      this.proceedEmit();
    } else {
      setTimeout(() => this.valueToDate(this.value as string), 0);
    }
  };

  private updateErrorStatus = (status: boolean, code: ErrorsCodes = null): void => {
    let errorDetails: IErrorDetails = { status: status };
    if (status) {
      errorDetails.code = code;
      switch (code) {
        case ErrorsCodes.MIN_MAX:
          errorDetails.description = 'min value can not be greater then max';
          break;
        case ErrorsCodes.OUT_OF_RANGE:
          errorDetails.description = 'out of min/max range';
          break;
        case ErrorsCodes.NOT_EXCLUDED:
          errorDetails.description = 'value must be excluded';
          break;
        case ErrorsCodes.INVALID_VALUE:
        default:
          errorDetails.description = 'invalid time value';
          break;
      }
    }

    if (!(this.hasError === errorDetails.status && this.lastKnownErrorCode === errorDetails.code)) {
      this.hasError = errorDetails.status;
      this.lastKnownErrorCode = errorDetails.code;
      this.emitEvent(ComponentEvents.ERROR_DETECTED, {
        detail: {
          error: errorDetails,
        },
      });
    }
  };

  updateScrollPosition = (): boolean => {
    setTimeout(() => {
      const listEl = this.element.querySelector('.dropdown > ul');
      const activeEl = listEl.querySelector('li.active');

      if (activeEl) {
        const scrollTop = (activeEl as HTMLElement).offsetTop - (activeEl.parentElement as HTMLElement).offsetTop;
        (listEl as HTMLElement).style.scrollBehavior = scrollTop < smoothScrollHeight ? 'smooth' : 'auto';
        (activeEl.parentElement as HTMLElement).scrollTop = scrollTop;
      }
    }, 0);
    return true;
  };

  get iconClockCssClass(): string {
    return (!this.value || this.hasError) && this.hasCurrentTimeControl && !this.disabled && !this.readonly
      ? 'cursor-pointer color-link'
      : `events-none ${this.disabled ? 'text-muted' : ''}`;
  }

  get clockCssClass(): string {
    const icon = 'mdi-clock-outline';
    const diff = 12;

    if (!this.date || this.hasError || isNaN(this.date.getTime())) return icon;

    try {
      const hours = this.date.getHours();
      const numeral = [...this.numerals][hours >= diff ? hours - diff : hours];
      return `mdi-clock-time-${numeral}-outline`;
    } catch (e) {
      return icon;
    }
  }

  get canBeEditable(): boolean {
    return numberingSystems.includes(this.resolvedOptions?.numberingSystem);
  }

  @computedFrom('expanded', 'inputOnly', 'readonly', 'presets.length')
  get canBeExpanded(): boolean {
    return this.expanded && !this.inputOnly && !this.readonly && this.presets.length && this.updateScrollPosition();
  }
}
