import { bindable } from 'aurelia-framework';

import { CoreComponent } from './base/core-component';

export class GtToast extends CoreComponent {
  @bindable message: string;
  @bindable duration: number = 2100;

  public isActive: boolean = false;
  private timeout: any;

  messageChanged(newValue: string, oldValue: any) {
    if (newValue) this.isActive = true;
    if (this.timeout) clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.isActive = false;
      clearTimeout(this.timeout);
    }, this.duration);
  }
}
