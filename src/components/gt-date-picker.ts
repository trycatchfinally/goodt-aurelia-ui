import { bindable, computedFrom } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

import { numberingSystems } from '../bcp47';
import { ComponentEvents, ErrorsCodes, SwipeDirection } from '../constants';
import {
  ICalendar,
  ICalendarDay,
  ICalendarEmpty,
  ICalendarMonth,
  ICalendarParams,
  ICalendarWeek,
  IErrorDetails,
  INameValue,
} from '../interfaces';
import {
  convertFromNumberingSystem,
  findParentElement,
  formatDateLabel,
  multiSplit,
  touchDevice,
} from './base/helpers';
import { PickerInput } from './base/picker-input';

enum FormatterTypes {
  YEAR_ONLY,
  DAY_ONLY,
  MONTH_ONLY,
  WEEKDAY_SHORT,
}

enum Direction {
  UP = -1,
  DOWN = 1,
}

enum SelectorName {
  CALENDAR = '.date-picker-calendar',
  YEARS = '.date-picker-years',
}

enum KeyControl {
  ARROW_DOWN = 'ArrowDown',
  ARROW_UP = 'ArrowUp',
  PAGE_DOWN = 'PageDown',
  PAGE_UP = 'PageUp',
  ESCAPE = 'Escape',
  ENTER = 'Enter',
}

type DatePickerHelperFormatter = {
  [key in FormatterTypes]: Intl.DateTimeFormat;
};

type DatePickerScrollableType = 'calendar' | 'years';

// todo custom formatter from/to/mask
export class GtDatePicker extends PickerInput {
  // https://github.com/tc39/ecma402/issues/6
  // https://github.com/FrankYFTang/proposal-intl-locale-info/
  @bindable firstDayOfWeek: number = 0;
  @bindable min: string; // yyyy-mm-dd
  @bindable max: string; // yyyy-mm-dd
  @bindable excludedDates: string[] = []; // yyyy-mm-dd
  @bindable excludedWeekDays: number[] = [];
  @bindable hasTodayControl: boolean = true;
  @bindable forceGregory: boolean = true;
  @bindable locale: string;
  @bindable value: string | Date;

  inputEl: HTMLInputElement;

  private lastKnownDateTimeFormatParts: Intl.DateTimeFormatPart[] = [];
  private lastKnownErrorCode: ErrorsCodes;
  private calendarFormatter: DatePickerHelperFormatter;
  private gregorianCalendarValue: string = 'gregory';
  private delay = 1;
  private keyHandlerBound: any;

  // calendar
  private calendarParams: ICalendarParams;
  private calendar: ICalendar;
  private years: INameValue[] = [];
  private yearsRange: number = 25;
  private monthRange: number;

  private daysInWeek: number = 7;

  bind() {
    super.bind();

    this.keyHandlerBound = this.keyHandler.bind(this);
    PLATFORM.global.addEventListener('keydown', this.keyHandlerBound);

    if (!this.locale)
      this.locale = PLATFORM.global.navigator.language || (PLATFORM.global.navigator as any)?.userLanguage;
    if (this.value) {
      const timestamp: number = Date.parse(this.value?.toString());
      if (isNaN(timestamp) === false) {
        this.date = this.normalizeDate(new Date(timestamp));
      }
    }
    if (this.firstDayOfWeek < 0 || this.firstDayOfWeek > 6) this.firstDayOfWeek = 0;
    this.monthRange = this.yearsRange * (touchDevice ? 0.8 : 1.2);
    this.calendarParams = {
      isScrolling: false,
      initialized: true,
      date: this.initializeDate(),
    };
    this.innerFormatter = new Intl.DateTimeFormat('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false,
    });
    this.init();
  }

  unbind() {
    super.unbind();
    PLATFORM.global.removeEventListener('keydown', this.keyHandlerBound);
  }

  private keyHandler = (e: KeyboardEvent) => {
    if (!this.expanded) return true;

    const yearsEl = this.element.querySelector(SelectorName.YEARS)?.parentElement;
    const calendarEl = this.element.querySelector(SelectorName.CALENDAR)?.parentElement;

    if (calendarEl && yearsEl && (<any>Object).values(KeyControl).includes(e.key)) {
      e.preventDefault();

      switch (e.key) {
        case KeyControl.ARROW_DOWN:
          this.scrollProceed(Direction.DOWN, 'calendar', calendarEl);
          break;
        case KeyControl.ARROW_UP:
          this.scrollProceed(Direction.UP, 'calendar', calendarEl);
          break;
        case KeyControl.PAGE_DOWN:
          this.scrollProceed(Direction.DOWN, 'years', yearsEl);
          break;
        case KeyControl.PAGE_UP:
          this.scrollProceed(Direction.UP, 'years', yearsEl);
          break;
        case KeyControl.ESCAPE:
          this.expanded = false;
          this.inputEl.blur();
          break;
      }
    }
    return true;
  };

  valueChanged(newValue: any, oldValue: any) {
    this.valueChangeDelegate(newValue);
  }

  localeChanged(newValue: string, oldValue: string) {
    this.init();
  }

  forceGregoryChanged(newValue: boolean, oldValue: boolean) {
    this.init();
  }

  minChanged(newValue: string, oldValue: string) {
    this.init();
  }

  maxChanged(newValue: string, oldValue: string) {
    this.init();
  }

  excludedChanged(newValue: string[], oldValue: string[]) {
    this.init();
  }

  async valueChangeDelegate(value: string | Date): Promise<void> {
    if (value === this.value) return Promise.resolve();

    this.updateErrorStatus(false);
    let date = value instanceof Date ? value : this.valueToDate(value);

    await this.updateDate(date);
    this.calendarParams.date = date;
    this.calendarParams.estimated = date;

    if (this.calendarParams.initialized) {
      this.initCalendar(this.calendarParams.date?.getFullYear());
    } else {
      this.calendarParams.initialized = true;
    }
    return Promise.resolve();
  }

  setToday = (): void => {
    this.updateErrorStatus(false);

    const date = this.normalizeDate(new Date());
    this.validate(date);

    if (!this.hasError) this.selectDate(date);
  };

  selectDate = (date: Date): void => {
    if (date.getTime() === this.date?.getTime()) return;

    this.calendarParams.initialized = date.getMonth() !== this.calendarParams.estimated?.getMonth();
    setTimeout(() => {
      this.valueChangeDelegate(date);
      this.inidicateSelectionByVibration();
    }, this.delay);
  };

  selectYear = (year: number): void => {
    if (this.calendarParams.estimated.getMonth() === 0 && this.calendarParams.estimated.getFullYear() === year) return;

    let month = this.isGregorian ? 0 : this.calendarParams.date.getMonth();
    let day = this.isGregorian ? 1 : this.calendarParams.date.getDate();
    let date = new Date(year, month, day);

    this.calendarParams.date = date;
    this.calendarParams.estimated = date;
    this.initCalendar(year);
    this.inidicateSelectionByVibration();
  };

  onFocus(e: FocusEvent): void {
    if (!this.value) this.date = null;

    let date = this.initializeDate();
    this.calendarParams.date = date;
    this.calendarParams.estimated = date;
    this.calendarParams.isScrolling = false;

    this.calendarParams.calendar = {
      index: null,
    };
    this.calendarParams.years = {
      elHeight: this.calendarParams.years?.elHeight,
    };

    this.expanded = true;
    this.initCalendar(date.getFullYear());
  }

  onChange(e: Event): void {
    e.preventDefault();
    this.updateDate(this.valueToDate((e.target as HTMLInputElement).value));
  }

  onKeyPress(e: KeyboardEvent): boolean {
    if (e.key === KeyControl.ENTER) {
      this.inputEl.blur();
      this.expanded = false;
    }
    return true;
  }

  onScroll = (e: WheelEvent, type: DatePickerScrollableType): boolean => {
    const parentElement: HTMLElement = findParentElement(e.target as HTMLElement, 'gt-grid-column');
    if (!parentElement || this.calendarParams.isScrolling) return false;

    this.scrollProceed((e as WheelEvent).deltaY > 0 ? Direction.DOWN : Direction.UP, type, parentElement);
    return false;
  };

  onSwipeDetect = (direction: SwipeDirection, type: DatePickerScrollableType): boolean => {
    if (!touchDevice) return false;

    this.inputEl.blur();
    if ([SwipeDirection.UP, SwipeDirection.DOWN].includes(direction))
      this.scrollProceed(direction === SwipeDirection.UP ? Direction.DOWN : Direction.UP, type);
    return false;
  };

  private scrollProceed = (direction: Direction, type: DatePickerScrollableType, element: HTMLElement = null): void => {
    if (this.calendarParams.isScrolling || !['calendar', 'years'].includes(type)) return;
    if (!element)
      element = this.element.querySelector(type === 'calendar' ? SelectorName.CALENDAR : SelectorName.YEARS)
        .parentElement;

    this.calendarParams.isScrolling = true;

    const scrollResetDelay = touchDevice ? 150 : 250;
    const parentScrollTop = element.scrollTop;
    const parentOffsetHeight = element.offsetHeight;
    let delta: number;

    const unlockScroll = () => {
      setTimeout(() => {
        this.calendarParams.initialized = true;
        this.calendarParams.isScrolling = false;
      }, scrollResetDelay);
    };

    const updateCalendar = () => {
      this.calendarParams.initialized = false;
      this.calendarParams.date = new Date(this.calendarParams.estimated.getTime());
      this.initCalendar(this.calendarParams.date.getFullYear());

      let index = this.calendar.months.findIndex(
        (month) => month.label === formatDateLabel(this.calendarParams.estimated),
      );

      setTimeout(() => {
        scrollMonth(index);
        scrollYear();
      }, this.delay);
    };

    const scrollMonth = (index: number) => {
      if (!this.calendar.months[index]?.label) return;

      this.calendarParams.calendar.index = index;
      const monthEl: HTMLElement = this.element.querySelector(`[data-month="${this.calendar.months[index].label}"]`);
      const offsetParentEl: HTMLElement = (monthEl as HTMLElement).offsetParent as HTMLElement;

      offsetParentEl.scrollTop = monthEl.offsetTop - (offsetParentEl.offsetHeight - monthEl.offsetHeight) / 2;
    };

    const scrollYear = () => {
      let index: number;
      let deltaByMonth = 0;
      const yearsEl = this.element.querySelector(SelectorName.YEARS);
      const yearsContainerEl = (yearsEl as HTMLElement)?.parentElement;

      if (this.isGregorian) {
        deltaByMonth = Math.floor(
          (this.calendarParams.estimated?.getMonth() / 12) * this.calendarParams.years.elHeight * direction,
        );
        index = this.years.findIndex((year) => year.value === this.calendarParams.estimated.getFullYear());
      } else {
        index = this.years.findIndex(
          (year) => year.name === this.calendar.months[this.calendarParams.calendar.index].year,
        );
      }

      if (!yearsContainerEl) return;
      if (index === -1) {
        updateCalendar();
      } else {
        yearsContainerEl.scrollTop =
          this.calendarParams.years.elHeight *
            (index - Math.floor(yearsContainerEl.offsetHeight / this.calendarParams.years.elHeight / 2)) +
          Math.abs(deltaByMonth);
      }
    };

    switch (type) {
      case 'calendar':
        let index = this.calendarParams.calendar.index + direction;
        let estimatedDate = new Date(this.calendarParams.estimated.getTime());

        estimatedDate.setDate(1);
        estimatedDate.setMonth(estimatedDate.getMonth() + direction);
        this.calendarParams.estimated = estimatedDate;

        if (this.calendar.months[index]) {
          scrollMonth(index);
          scrollYear();
        } else {
          updateCalendar();
        }

        unlockScroll();
        break;

      case 'years':
        let step = Math.floor(this.yearsRange / 2);
        let newScrollTop: number;
        const mustBeUpdated: boolean =
          (direction === Direction.DOWN
            ? element.scrollHeight - (parentScrollTop + parentOffsetHeight)
            : parentScrollTop) < parentOffsetHeight;
        delta = this.calendarParams.years.elHeight * direction;

        const updateOffset = (scrollTop: number) => {
          const updatedScrollTop = scrollTop + delta;

          let estimatedYear =
            this.years[
              Math.round((updatedScrollTop + parentOffsetHeight / 2) / this.calendarParams.years.elHeight + 0.5) - 1
            ]?.value ?? this.calendarParams.date.getFullYear();
          let index: number;

          this.calendarParams.estimated.setFullYear(estimatedYear);
          element.scrollTop = updatedScrollTop;

          if (this.isGregorian) {
            index = this.calendarParams.calendar.index + direction * 12;
            if (this.calendar.months[index]) {
              scrollMonth(index);
            } else {
              let estimatedDate = new Date(this.calendarParams.estimated.getTime());
              estimatedDate.setDate(1);

              this.initMonths(estimatedDate);
              index = this.calendar.months.findIndex((month) => month.label === formatDateLabel(estimatedDate));
              setTimeout(() => scrollMonth(index), this.delay);
            }
          } else {
            this.initMonths(new Date(this.calendarParams.estimated.getTime()));
            index = this.calendar.months.findIndex(
              (month) => month.year === this.years.find((year) => year.value === estimatedYear).name,
            );
            this.calendarParams.initialized = true;
            setTimeout(() => scrollMonth(index), this.delay);
          }
          unlockScroll();
        };

        if (mustBeUpdated) {
          this.calendarParams.initialized = false;
          this.calendarParams.date = new Date(
            this.calendarParams.date.getFullYear() + step * direction,
            this.calendarParams.date.getMonth(),
            this.calendarParams.date.getDate(),
          );

          this.initCalendar(this.calendarParams.date.getFullYear());
          newScrollTop = parentScrollTop - this.calendarParams.years.elHeight * step * direction;
          element.scrollTop = newScrollTop;

          setTimeout(() => updateOffset(newScrollTop), this.delay);
        } else {
          updateOffset(element.scrollTop);
        }
        break;
    }
  };

  private emitDelegate = () => {
    const hasValue = this.value && (this.value as string).trim().length;
    const parts = hasValue ? this.valueFormatter.formatToParts(this.date) : [];

    if (JSON.stringify(parts) !== JSON.stringify(this.lastKnownDateTimeFormatParts)) {
      this.emit(parts);
    }
  };

  private emit = (data: any): void => {
    this.lastKnownDateTimeFormatParts = data;
    this.emitEvent(ComponentEvents.VALUE_CHANGED, {
      detail: {
        value: data,
      },
    });
  };

  private init(): void {
    this.initFormatter();
    this.initHelperFormatters();
    this.updateErrorStatus(false);
    this.resolvedOptions = new Intl.DateTimeFormat(this.locale).resolvedOptions();

    if (this.value && typeof this.value === 'string' && !this.isGregorian) {
      if (this.date) {
        this.value = this.dateToValue(this.date);
      } else {
        this.updateErrorStatus(true, ErrorsCodes.INVALID_TYPE);
      }
    }

    if (this.min && this.max && this.min >= this.max) {
      this.updateErrorStatus(true, ErrorsCodes.MIN_MAX);
    }

    // calendar
    this.initCalendar(this.calendarParams.date.getFullYear());

    // placeholder
    this.placeholder = this.dateToValue(this.normalizeDate(new Date()));

    // finalize
    this.updateDate(this.date, false);
  }

  private initCalendar = (year: number = null): void => {
    year = year ?? (this.date?.getFullYear() || new Date().getFullYear());

    // years
    const startRangeYear = year - Math.floor(this.yearsRange / 2);
    this.years = [...new Array(this.yearsRange)]
      .map((_: any, i: number) => startRangeYear + i)
      .map(
        (year: number): INameValue => {
          return {
            name: this.calendarFormatter[FormatterTypes.YEAR_ONLY].format(new Date(year, 0)),
            value: year,
          };
        },
      );

    // calendar
    let initialDate =
      this.calendarParams.date?.getFullYear() === year ? this.calendarParams.date : new Date(year, 0, 1, 0, 0, 0, 0);

    this.initMonths(initialDate);
    if (this.expanded) this.updateScrollPosition();
  };

  private initMonths = (initialDate: Date) => {
    const halfPeriod = Math.round(this.monthRange / 2);
    const initialWeekDay = initialDate.getDay();
    const isGregorianCalendar = this.isGregorianByLocale || this.forceGregory;
    const dayDelta = 60 * 60 * 24 * 1000;

    let months: ICalendarMonth[] = [];
    let weekdays = [...new Array(this.daysInWeek)].map((_, i) => i);
    let startYear = initialDate.getFullYear() - Math.floor(halfPeriod / 12);
    let startMonth = initialDate.getMonth() - (halfPeriod % 12);

    if (startMonth < 0) {
      --startYear;
      startMonth += 12;
    }
    while (weekdays[0] !== this.firstDayOfWeek) {
      let first = weekdays.splice(0, 1);
      weekdays.push(first[0]);
    }

    const weekdayNames = weekdays.map((v) => {
      const delta = v - initialWeekDay;
      return this.calendarFormatter[FormatterTypes.WEEKDAY_SHORT].format(
        new Date(initialDate.getTime() + delta * dayDelta),
      );
    });

    const processCalendarDay = (date: Date): ICalendarDay => {
      let label = formatDateLabel(date);

      return {
        name: this.calendarFormatter[FormatterTypes.DAY_ONLY].format(date),
        value: date,
        empty: false,
        disabled:
          this.excludedWeekDays?.includes(date.getDay() % this.daysInWeek) ||
          (this.min ? label < this.min : false) ||
          (this.max ? label > this.max : false) ||
          (this.excludedDates.length ? this.excludedDates.includes(label) : false),
      };
    };
    const processCalendarWeeks = (leftOffset: number, days: ICalendarDay[]): ICalendarWeek[] => {
      let daysWithOffsets: ICalendarDay[] | ICalendarEmpty[] = [];
      let weeks: ICalendarWeek[] = [];

      daysWithOffsets = [
        ...[...new Array(leftOffset)].map(() => {
          return { empty: true };
        }),
        ...days,
      ];
      const rightOffset = this.daysInWeek - (daysWithOffsets.length % this.daysInWeek);

      if (rightOffset > 0 && rightOffset < this.daysInWeek) {
        daysWithOffsets = [
          ...daysWithOffsets,
          ...[...new Array(rightOffset)].map(() => {
            return { empty: true };
          }),
        ];
      }

      weeks = [...new Array(Math.ceil(daysWithOffsets.length / this.daysInWeek))].map((_, i) => {
        return {
          days: daysWithOffsets.slice(i * this.daysInWeek, (i + 1) * this.daysInWeek),
        };
      });

      return weeks;
    };
    const processCalendarMonth = (weeks: ICalendarWeek[], startDate: Date): ICalendarMonth => {
      return {
        title: this.calendarFormatter[FormatterTypes.MONTH_ONLY].format(startDate),
        year: this.calendarFormatter[FormatterTypes.YEAR_ONLY].format(startDate),
        weeks,
        label: formatDateLabel(startDate),
      };
    };

    if (isGregorianCalendar) {
      for (let i = 0; i < this.monthRange; i++) {
        let d = 1;
        const monthStartDate = new Date(startYear, startMonth + i, d, 0, 0, 0, 0);
        let nextDate = monthStartDate;
        let days: ICalendarDay[] = [];
        const leftOffset: number = weekdays.findIndex((v) => v === monthStartDate.getDay());

        while (monthStartDate.getMonth() === nextDate.getMonth()) {
          days.push(processCalendarDay(nextDate));
          ++d;
          nextDate = new Date(startYear, startMonth + i, d, 0, 0, 0, 0);
        }
        months.push(processCalendarMonth(processCalendarWeeks(leftOffset, days), monthStartDate));
      }
    } else {
      let d = 1;
      let mi = 0;
      let monthStartDate = new Date(startYear, startMonth, d, 0, 0, 0, 0);
      let nextDate: Date;
      let monthTitle = this.calendarFormatter[FormatterTypes.MONTH_ONLY].format(monthStartDate);
      nextDate = monthStartDate;

      while (this.calendarFormatter[FormatterTypes.MONTH_ONLY].format(nextDate) === monthTitle) {
        monthStartDate = nextDate;
        --d;
        nextDate = new Date(startYear, startMonth, d, 0, 0, 0, 0);
      }

      nextDate = monthStartDate;
      while (mi !== this.monthRange) {
        d = 0;
        let days: ICalendarDay[] = [];
        const leftOffset: number = weekdays.findIndex((v) => v === monthStartDate.getDay());
        monthTitle = this.calendarFormatter[FormatterTypes.MONTH_ONLY].format(monthStartDate);

        while (this.calendarFormatter[FormatterTypes.MONTH_ONLY].format(nextDate) === monthTitle) {
          days.push(processCalendarDay(nextDate));
          ++d;
          nextDate = new Date(nextDate.getTime() + dayDelta);
        }
        months.push(processCalendarMonth(processCalendarWeeks(leftOffset, days), monthStartDate));

        ++mi;
        monthStartDate = nextDate;
      }
    }

    this.calendar = {
      months,
      weekdays: weekdayNames,
    };
  };

  private normalizeDate = (date: Date): Date => {
    date.setHours(0, 0, 0, 0);
    
    return date;
  };

  private initFormatter() {
    const options: Intl.DateTimeFormatOptions = {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
    };
    this.valueFormatter = new Intl.DateTimeFormat([this.normalizedLocale, this.fallbackLocale], options);
  }

  private initHelperFormatters(): void {
    if (!this.locale) return;

    const formatter = (options: Intl.DateTimeFormatOptions = {}) =>
      new Intl.DateTimeFormat([this.normalizedLocale, this.fallbackLocale], options);
    this.calendarFormatter = {
      [FormatterTypes.YEAR_ONLY]: formatter({ year: 'numeric' }),
      [FormatterTypes.DAY_ONLY]: formatter({ day: 'numeric' }),
      [FormatterTypes.MONTH_ONLY]: formatter({ month: 'long' }),
      [FormatterTypes.WEEKDAY_SHORT]: formatter({ weekday: 'short' }),
    };

    if (this.years?.length) {
      this.years.forEach((y) => {
        y.name = this.calendarFormatter[FormatterTypes.YEAR_ONLY].format(new Date(y.value, 0));
      });
    }
  }

  private dateToValue(date: Date): string {
    return isNaN(date.getTime()) ? '' : date && this.valueFormatter ? this.valueFormatter.format(date) : '';
  }

  private valueToDate(value: string): Date | null {
    if (!value) return null;
    if (!this.isGregorian) return this.date;
    let date: Date | boolean;

    value = value.trim();

    // check for yyyy-mm-dd
    if (this.parseCalendarDate(value)) date = this.parseCalendarDate(value, true);

    // check for yyyymmdd
    if (!date && this.parseShortCalendarDate(value)) date = this.parseShortCalendarDate(value, true);

    // parse localized value
    if (!date) {
      let parts = this.valueFormatter.formatToParts(new Date());
      let splitters = [...new Set(parts.filter((fp) => fp.type === 'literal').map((data) => data.value))];
      let suggestedDateParts = multiSplit(value, splitters);

      let extracted = parts
        .filter((fp) => ['year', 'month', 'day'].includes(fp.type))
        .map((fp) => {
          return {
            ...fp,
            ...{
              value: convertFromNumberingSystem(fp.value, this.resolvedOptions.numberingSystem),
            },
          };
        });

      if (suggestedDateParts.length !== extracted.length) {
        this.hasError || this.updateErrorStatus(true, ErrorsCodes.INVALID_VALUE);
      } else {
        let findPart = (type: string) =>
          parseInt(
            convertFromNumberingSystem(
              suggestedDateParts[extracted.findIndex((fp) => fp.type === type)],
              this.resolvedOptions.numberingSystem,
            ),
          );
        let year: number = findPart('year');
        let month: number = findPart('month') - 1;
        let day: number = findPart('day');
        date = new Date(year, month, day, 0, 0, 0, 0);
      }
    }

    try {
      this.validate(date);
    } catch (e) {
      this.updateErrorStatus(true, ErrorsCodes.INVALID_VALUE);
    }

    return this.hasError && !date ? null : this.normalizeDate(date as Date);
  }

  private async updateDate(date: Date, emit: boolean = true): Promise<void> {
    if (!date) {
      emit && this.emit([]);
      return Promise.resolve();
    }

    if (this.date?.getTime() != date.getTime()) this.date = date;
    this.value = this.dateToValue(this.date);

    if (emit) {
      this.emitDelegate();
      return Promise.resolve();
    } else {
      setTimeout(() => {
        this.valueToDate(this.value as string);
        return Promise.resolve();
      }, this.delay);
    }
  }

  private updateErrorStatus = (status: boolean, code: ErrorsCodes = null): void => {
    let errorDetails: IErrorDetails = { status: status };
    status = Boolean(status);

    if (status) {
      errorDetails.code = code;
      switch (code) {
        case ErrorsCodes.MIN_MAX:
          errorDetails.description = 'min value can not be greater then max';
          break;
        case ErrorsCodes.OUT_OF_RANGE:
          errorDetails.description = 'out of min/max range';
          break;
        case ErrorsCodes.NOT_EXCLUDED:
          errorDetails.description = 'value must be excluded';
          break;
        case ErrorsCodes.INVALID_TYPE:
          errorDetails.description = 'for non-gregorian calendar value must be type of `Date` only';
          break;
        case ErrorsCodes.INVALID_VALUE:
        default:
          errorDetails.description = 'invalid date value';
          break;
      }
    }

    if (!(this.hasError === errorDetails.status && this.lastKnownErrorCode === errorDetails.code)) {
      this.hasError = errorDetails.status;
      this.lastKnownErrorCode = errorDetails.code;
      this.emitEvent(ComponentEvents.ERROR_DETECTED, {
        detail: {
          error: errorDetails,
        },
      });
    }
  };

  updateScrollPosition = (): boolean => {
    if (this.calendarParams.isScrolling) return true;
    this.calendarParams.isScrolling = true;

    const rules = [SelectorName.YEARS, SelectorName.CALENDAR];
    let selectors = [];

    this.calendarParams.date = this.calendarParams.date ?? this.initializeDate();
    this.calendarParams.estimated = new Date(this.calendarParams.date.getTime());

    let interval = setInterval(() => {
      selectors = rules.map((_) => this.element.querySelector(_));

      if (selectors.map((selector) => selector?.children?.length).filter((_) => _).length === rules.length) {
        const yearsEl: HTMLElement = selectors[0];
        const calendarEl: HTMLElement = selectors[1];

        const monthIndex = this.calendar.months.findIndex((month) =>
          month.weeks.some((week) =>
            week.days.some(
              (day) => formatDateLabel((day as ICalendarDay)?.value) === formatDateLabel(this.calendarParams.date),
            ),
          ),
        );
        const yearIndex = this.years.findIndex((year) => year.name === this.calendar.months[monthIndex].year);

        const initialMonth = this.calendarParams.date.getMonth();
        const monthLabel = this.calendar.months[monthIndex].label;

        const initialYearEl = yearsEl?.querySelector(`[data-index="${yearIndex}"]`);
        const initMonthEl = calendarEl?.querySelector(`[data-month="${monthLabel}"]`);

        if (initialYearEl) {
          // if (!yearsEl.style.width) yearsEl.style.width = `${(yearsEl.offsetWidth + 2) * 100 / PLATFORM.global.innerWidth}vw`;
          if (!this.calendarParams.years.elHeight)
            this.calendarParams.years.elHeight = (initialYearEl as HTMLElement).offsetHeight;

          const scrollTop =
            (initialYearEl as HTMLElement).offsetTop - (initialYearEl.parentElement as HTMLElement).offsetTop;
          const yearsContainer = yearsEl.parentElement as HTMLElement;
          const deltaByMonth = this.isGregorian ? (initialMonth / 12) * this.calendarParams.years.elHeight : 0;

          yearsContainer.scrollTop =
            scrollTop -
            Math.floor(yearsContainer.offsetHeight / this.calendarParams.years.elHeight / 2) *
              this.calendarParams.years.elHeight +
            deltaByMonth;
        }

        if (initMonthEl) {
          const calendarContainer = calendarEl.parentElement as HTMLElement;

          this.calendarParams.calendar.index = this.calendar.months.findIndex((month) => month.label === monthLabel);
          calendarContainer.scrollTop =
            (initMonthEl as HTMLElement).offsetTop -
            (calendarContainer.offsetHeight - (initMonthEl as HTMLElement).offsetHeight) / 2;
        }

        if (this.calendarParams.initialized) {
          this.calendarParams.isScrolling = false;
        } else {
          setTimeout(() => {
            this.calendarParams.initialized = true;
            this.calendarParams.isScrolling = false;
          }, 100);
        }

        clearInterval(interval);
      }
    });
    return true;
  };

  // yyyymmdd
  private parseShortCalendarDate = (dateString: string, result: boolean = false): boolean | Date => {
    let regEx = /^\d{8}$/;
    if (!dateString.match(regEx)) return false;

    let y = dateString.slice(0, 4);
    let m = dateString.slice(4, 6);
    let d = dateString.slice(6);

    let dt = new Date(parseInt(y), parseInt(m) - 1, parseInt(d), 0, 0, 0, 0);
    let dNum = dt.getTime();

    if (!dNum && dNum !== 0) return false;
    return result ? dt : true;
  };

  // yyyy-mm-dd
  private parseCalendarDate = (dateString: string, result: boolean = false): boolean | Date => {
    let regEx = /^\d{4}-\d{2}-\d{2}$/;

    if (!dateString.match(regEx)) return false;
    let dt = new Date(dateString);
    let dNum = dt.getTime();

    if (!dNum && dNum !== 0) return false;
    return dt.toISOString().slice(0, 10) === dateString ? (result ? dt : true) : false;
  };

  private validate = (date: Date | boolean): void => {
    const label = formatDateLabel((date as unknown) as Date);

    // invalid date value
    this.hasError || (!date && this.updateErrorStatus(true, ErrorsCodes.INVALID_VALUE));

    // min/max values
    this.hasError || this.updateErrorStatus(this.min && this.max && this.min >= this.max, ErrorsCodes.MIN_MAX);

    // min/max, out of range
    this.hasError ||
      (date &&
        (date as Date)?.getTime() &&
        this.updateErrorStatus(
          (this.min ? this.min > label : false) || (this.max ? this.max < label : false),
          ErrorsCodes.OUT_OF_RANGE,
        ));

    // excludes
    this.hasError ||
      (date &&
        (date as Date)?.getTime() &&
        this.updateErrorStatus(
          this.excludedWeekDays?.includes(((date as unknown) as Date).getDay() % this.daysInWeek) ||
            (this.excludedDates.length ? this.excludedDates.includes(label) : false),
          ErrorsCodes.NOT_EXCLUDED,
        ));
  };

  private initializeDate = (): Date => {
    if (this.date) return this.date;

    const today = new Date();
    return new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);
  };

  private inidicateSelectionByVibration = (duration = 100) => {
    if (touchDevice) {
      try {
        PLATFORM.global.navigator.vibrate(duration);
      } catch (e) {}
    }
  };

  get iconClockCssClass(): string {
    return this.hasTodayControl && !this.disabled && !this.readonly
      ? 'cursor-pointer color-link'
      : `events-none ${this.disabled ? 'text-muted' : ''}`;
  }

  get isGregorianByLocale(): boolean {
    return this.resolvedOptions?.calendar === this.gregorianCalendarValue;
  }

  get isGregorian(): boolean {
    return this.isGregorianByLocale || this.forceGregory;
  }

  get canBeEditable(): boolean {
    return numberingSystems.includes(this.resolvedOptions?.numberingSystem);
  }

  @computedFrom('expanded', 'inputOnly', 'readonly')
  get canBeExpanded(): boolean {
    return (
      this.expanded &&
      !this.inputOnly &&
      !this.readonly &&
      this.calendar.months.length &&
      this.years.length &&
      this.updateScrollPosition()
    );
  }

  @computedFrom('locale', 'forceGregory')
  get normalizedLocale(): string {
    if (!this.forceGregory) return this.locale;
    if (this.isGregorianByLocale) this.locale;

    let locale = new (Intl as any).Locale(this.locale, {
      calendar: this.gregorianCalendarValue,
    });
    return locale.toString();
  }
}
