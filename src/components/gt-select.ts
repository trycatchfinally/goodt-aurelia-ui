import { bindable } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

import { smoothScrollHeight } from '../constants';
import { INameValue } from '../interfaces';
import { DropdownBasedComponent } from './base/dropdown-based-component';
import { uniqueName } from './base/helpers';

export class GtSelect extends DropdownBasedComponent {
  @bindable items: INameValue[];
  @bindable value: any | any[];
  @bindable name: string = uniqueName();
  @bindable placeholder: string = '';
  @bindable wrapperClass: string | string[];
  @bindable dropdownClass: string | string[];
  @bindable multiple: boolean = false;
  @bindable hasResetControl: boolean = true;
  @bindable disabled: boolean = false;

  // multiple mode only
  @bindable summary: string | boolean = '{n}';

  private selectField: HTMLInputElement;
  private selectName: string = uniqueName();
  private selectFieldStyles: any;

  bind() {
    super.bind();
    if (this.multiple && !Array.isArray(this.value)) this.value = [this.value];
  }

  attached() {
    const padding = parseInt(PLATFORM.global.getComputedStyle(this.selectField).getPropertyValue('padding-right')) ?? 0;
    this.selectFieldStyles = padding ? { paddingRight: `${Math.round(padding * 1.5)}px` } : {};
  }

  valueChanged(newValue: any | any[], oldValue: any) {
    if (!Array.isArray(newValue) && this.multiple) {
      this.value = [this.value];
      return;
    }
  }

  multipleChanged(newValue: boolean, oldValue: boolean) {
    this.value = newValue
      ? Array.isArray(this.value)
        ? [...this.value]
        : this.singleValueLength
        ? [this.value]
        : []
      : oldValue
      ? this.value?.[0] ?? ''
      : this.value;
  }

  onFocus(e: FocusEvent): void {
    this.expanded = true;
  }

  toggleExpanded(e: MouseEvent): void {
    this.expanded = !this.expanded;
  }

  updatedSelected() {
    this.value = this.multiple
      ? this.value?.length === this.items.length
        ? []
        : this.items.map((item) => item.value)
      : '';
  }

  selectItem(e: MouseEvent, item: INameValue): boolean {
    if (!e.detail) {
      if (this.multiple) {
        if (this.value.includes(item.value)) {
          this.value = this.value.filter((value: any) => item.value != value);
        } else {
          this.value.push(item.value);
        }
      } else {
        this.value = item.value;
        this.expanded = false;
      }
    }
    return true;
  }

  updateScrollPosition = (): boolean => {
    if (this.multiple) return true;

    setTimeout(() => {
      const listEl = this.element.querySelector('.dropdown > ul');
      const activeEl = listEl.querySelector(':checked')?.closest('li');

      if (activeEl) {
        const scrollTop = (activeEl as HTMLElement).offsetTop - (activeEl.parentElement as HTMLElement).offsetTop;
        (listEl as HTMLElement).style.scrollBehavior = scrollTop < smoothScrollHeight / 2 ? 'smooth' : 'auto';
        (activeEl.parentElement as HTMLElement).scrollTop = scrollTop;
      }
    }, 0);
    return true;
  };

  get summarized() {
    if (!this.singleValueLength) {
      return '';
    }

    let values = this.multiple ? [...this.value] : [this.value];
    let names =
      this.items
        .filter((item) => values.includes(item.value))
        .map((item) => item.name)
        .sort() || [];
    if (this.multiple) {
      return typeof this.summary === 'boolean'
        ? this.summary
          ? names.join(', ')
          : ''
        : `${this.placeholder} / ${this.summary.replace('{n}', names.length.toString())}`;
    } else {
      return names[0];
    }
  }

  get ctrlEnabled(): boolean {
    if (!this.multiple) return Boolean(this.singleValueLength) && this.hasResetControl && this.expanded;
    return this.expanded;
  }

  get ctrlCssClass(): string {
    if (this.multiple) {
      return this.value?.length === this.items.length ? 'mdi-close' : 'mdi-check';
    } else {
      return this.singleValueLength ? 'mdi-close' : '';
    }
  }

  get singleValueLength(): number {
    return this.value?.toString()?.length;
  }
}
