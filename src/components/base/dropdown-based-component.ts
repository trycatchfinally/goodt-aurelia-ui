import { PLATFORM } from 'aurelia-pal';

import { CoreComponent } from './core-component';

export class DropdownBasedComponent extends CoreComponent {
  protected expanded: boolean = false;
  private handlerBound: any;

  bind() {
    this.handlerBound = this.clickHandler.bind(this);
    PLATFORM.global.addEventListener('click', this.handlerBound);
  }

  unbind() {
    PLATFORM.global.removeEventListener('click', this.handlerBound);
  }

  private clickHandler = (e: Event) => {
    if (!this.element.contains((e as any).target) && this.expanded) {
      this.expanded = false;
    }
  };

  get isElementPositionHigh(): boolean {
    let dropdownEl = this.element.querySelector('.dropdown');
    if (!dropdownEl) return true;
    try {
      const rect = this.element.getBoundingClientRect();
      const defaultView = this.element.ownerDocument.defaultView;
      const outerHeight = defaultView.outerHeight;

      let result = outerHeight / 2 > rect.top + rect.height / 2;
      (dropdownEl.parentElement as HTMLElement).style.marginTop = result ? '' : `-${rect.height}px`;
      (dropdownEl as HTMLElement).style.visibility = '';

      return result;
    } catch (e) {
      (dropdownEl as HTMLElement).style.visibility = '';
      return true;
    }
  }

  get dropdownStyles(): any {
    return { visibility: 'hidden' };
  }
}
