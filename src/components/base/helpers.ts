import { PLATFORM } from 'aurelia-pal';

import { numberingSystems } from '../../bcp47';

/**
 * additional l18n for pickers
 * see also https://stackoverflow.com/a/8181459
 * @param value
 * @param numberingSystem
 */
export function convertFromNumberingSystem(value: string, numberingSystem: string): string {
  if (!numberingSystems.includes(numberingSystem)) return value;

  let summarized = summarizeNumberingSystem(numberingSystem);
  let replaced = value;

  summarized.forEach((collation, index) => {
    let re = new RegExp(collation, 'g');
    replaced = replaced.replace(re, index.toString());
  });
  return replaced;
}

/**
 * get pairs for alternate numbering system
 * @param numberingSystem
 */
export function summarizeNumberingSystem(numberingSystem: string): string[] {
  let f = new Intl.NumberFormat(`en-US-u-nu-${numberingSystem}`, {
    style: 'decimal',
  });
  let digits: string[] = [];
  for (let i = 0; i < 10; i++) {
    let digit = f.format(i);
    digits.push(digit);
  }
  return digits;
}

/**
 * remove timezone offset from date
 * @param date
 */
export function updateDateByTimezoneOffset(date: Date): Date {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
}

/**
 * format date to yyyy-mm-dd by default length
 * @param date
 * @param length
 */
export function formatDateLabel(date: Date, length: number = 10): string {
  try {
    return date ? updateDateByTimezoneOffset(date)?.toISOString().substring(0, length) : null;
  } catch (e) {
    return null;
  }
}

/**
 * input name
 * @param length
 */
export function uniqueName(length = 30) {
  return '_' + [...Array(length)].map(() => Math.random().toString(36)[2]).join('');
}

/**
 * multi split
 * @param str
 * @param tokens
 */
export function multiSplit(str: string | any, tokens: string[]): string[] {
  let token = tokens[0];
  for (let i = 1; i < tokens.length; i++) {
    str = str.split(tokens[i]).join(token);
  }
  return str.split(token).filter((_: string) => _?.toString()?.trim() !== '') ?? [];
}

/**
 * find parent node by tag name
 * @param element
 * @param tagName
 */
export function findParentElement(element: HTMLElement, tagName: string = ''): HTMLElement {
  try {
    const parentElement = element?.parentElement;

    return (parentElement as HTMLElement)?.tagName?.toLowerCase() === tagName.toLowerCase()
      ? parentElement
      : findParentElement(parentElement as HTMLElement, tagName);
  } catch (e) {
    return element;
  }
}

// check if screen supports touch events
export const touchDevice: boolean =
  'ontouchstart' in PLATFORM.global || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;

// mouse/touch events
export const mouseDown: string = touchDevice ? 'touchstart' : 'mousedown';
export const mouseOut: string = touchDevice ? 'touchcancel' : 'mouseout';
export const mouseUp: string = touchDevice ? 'touchend' : 'mouseup';
