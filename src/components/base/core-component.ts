import { autoinject, BindableProperty, TargetInstruction } from 'aurelia-framework';

@autoinject
export class CoreComponent {
  protected owningView: any;

  protected constructor(readonly element: Element, protected targetInstruction: TargetInstruction) {
    this.targetInstruction = targetInstruction;
  }

  protected created(owningView: any, myView: any) {
    if (!this.validContainer) {
      throw new Error(`Custom component ${this.element.tagName?.toLowerCase() ?? ''} cannot be containerless`);
    }

    const $el = this.element;
    const bindableProperties = (this.targetInstruction.elementInstruction.type as any)?.attributes;

    Object.keys(bindableProperties)?.map((p: string) => {
      const bindableProperty: BindableProperty = bindableProperties[p];
      const attr = (bindableProperty as any).attribute;
      const name = (bindableProperty as any).name;

      if ($el.hasAttribute(attr)) {
        const value = $el.getAttribute(attr);
        switch (value) {
          case 'null':
            this[name] = null;
            break;

          case 'undefined':
            this[name] = undefined;
            break;

          case 'false':
            this[name] = false;
            break;

          case 'true':
          case '':
          case null:
          case undefined:
            this[name] = true;
            break;

          default:
            if (isNaN(value as any)) {
              try {
                this[name] = JSON.parse(value);
              } catch (e) {
                this[name] = value;
              }
            } else {
              this[name] = (value as any) | 0;
            }
            break;
        }
      }
    });

    this.owningView = owningView;
  }

  checkRequired(attrs: string[]): void {
    const expressions = this.targetInstruction.expressions;
    const props = expressions.map((be) => (be as any)?.targetProperty);

    attrs.forEach((a) => {
      if (!props.includes(a)) {
        throw new Error(
          `${(this.element as any).tagName?.toLowerCase() ?? 'Custom'} component requires '${a}' binding`,
        );
      }
    });
  }

  protected emitEvent(eventName: string, params: any = {}) {
    this.element.dispatchEvent(new CustomEvent(eventName, { ...params, ...{ bubbles: true } }));
  }

  protected get validContainer() {
    return !(this.element instanceof Comment);
  }

  public get classList() {
    return this.element?.classList;
  }
}
