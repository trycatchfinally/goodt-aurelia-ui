import { bindable } from 'aurelia-framework';

import { DropdownBasedComponent } from './dropdown-based-component';
import { uniqueName } from './helpers';

export class PickerInput extends DropdownBasedComponent {
  @bindable inputOnly: boolean = false;
  @bindable hasResetControl: boolean = false;
  @bindable fallbackLocale: string = 'en-US';

  // input properties
  @bindable name: string = uniqueName();
  @bindable disabled: boolean = false;
  @bindable readonly: boolean = false;
  @bindable required: boolean = false;

  protected date: Date;
  protected valueFormatter: Intl.DateTimeFormat;
  protected innerFormatter: Intl.DateTimeFormat;
  protected resolvedOptions: Intl.ResolvedDateTimeFormatOptions;
  protected hasError: boolean = false;

  protected placeholder: string;
}
