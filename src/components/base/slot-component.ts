import { CoreComponent } from './core-component';

export class SlotComponent extends CoreComponent {
  protected validSlot = <any>null;

  protected attached() {
    if (this.validSlot) this.validateSlot();
  }

  private validateSlot() {
    if (!this.validSlot) return;

    const slots = (this.element as any).au.controller.view.slots;
    for (let slotName of Object.keys(slots)) {
      const slot = slots[slotName];
      if (slot.children?.length) {
        for (let child of slot.children) {
          if (!(child.au?.controller.viewModel instanceof this.validSlot || child instanceof Comment)) {
            throw new TypeError(
              `Illegal slot content: ${(this.element as any).tagName?.toLowerCase() ?? ''} must contain ${
                this.validSlot?.$resource ?? 'specified'
              } child only, see component documentation`,
            );
          }
        }
      }
    }
  }
}
