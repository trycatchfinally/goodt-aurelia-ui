import { bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

export class GtDetails extends SlotComponent {
  @bindable active: boolean = false;
  @bindable isRight: boolean = false;
  @bindable summary: string;
  @bindable iconClass: string;
  @bindable summaryClass: string;

  get iconCssClass() {
    let classes = [];

    if (this.iconClass) classes.push(this.iconClass);
    classes.push(this.active ? 'mdi-chevron-down' : 'mdi-chevron-right');

    return classes.join(' ');
  }

  get summaryCssClass() {
    let classes = [];

    if (this.summaryClass) classes.push(this.summaryClass);
    return classes.join(' ');
  }
}
