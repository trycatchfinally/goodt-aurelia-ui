import { bindable } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

export class GtShim extends SlotComponent {
  @bindable hoverOnly: boolean = false;
  @bindable overlayBgClass: string = '';

  overlayBgClassChanged(newValue: any, oldValue: any): void {
    this.overlayBgClass = newValue && newValue.indexOf('bg-') === 0 ? newValue : '';
  }
}
