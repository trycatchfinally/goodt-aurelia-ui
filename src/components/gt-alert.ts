import { bindable } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { SlotComponent } from './base/slot-component';

export class GtAlert extends SlotComponent {
  @bindable type: string;
  @bindable wrapperClass: string = '';
  @bindable isCloseable: boolean = true;

  private types: Set<string> = new Set(['success', 'warn', 'error']);

  get cssByType() {
    return [this.wrapperClass, this.type ? `alert-${this.type}` : ''].filter((c) => c).join(' ');
  }

  typeChanged(newValue: string, oldValue: any) {
    if (!this.types.has(newValue)) this.type = undefined;
  }

  close = () => {
    this.emitEvent(ComponentEvents.DEACTIVATE_READY);
  };
}
