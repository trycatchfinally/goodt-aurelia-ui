import { bindable } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { SlotComponent } from './base/slot-component';

export class GtChip extends SlotComponent {
  @bindable rounded: boolean = false;

  public removeChip() {
    this.emitEvent(ComponentEvents.DEACTIVATE_READY, {
      detail: {
        value: ((this.element as HTMLElement).innerText ?? '').trim(),
      },
    });
  }
}
