import { bindable, containerless } from 'aurelia-framework';

import { SlotComponent } from './base/slot-component';

@containerless
export class GtMenu extends SlotComponent {
  @bindable isSmall: boolean = false;
  @bindable isLeft: boolean = false;

  protected get cssClasses(): string {
    const map = new Map([
      ['isSmall', 'menu-small'],
      ['isLeft', 'menu-left'],
    ]);
    return [...map.keys()]
      .filter((p) => this[p])
      .map((p) => map.get(p))
      .join(' ');
  }

  protected get validContainer() {
    return true;
  }
}
