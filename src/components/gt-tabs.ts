import { BehaviorInstruction, bindable, BindingEngine, Container, processContent } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { SlotComponent } from './base/slot-component';

@processContent(GtTabs.processTabs)
export class GtTabs extends SlotComponent {
  @bindable public activeTabId: number = 0;
  @bindable public isCentered: boolean = false;
  @bindable public isRight: boolean = false;
  @bindable public grow: boolean = false;

  isRightChanged(newValue: boolean, oldValue: any) {
    if (newValue === true) this.isCentered = false;
  }

  isCenteredChanged(newValue: boolean, oldValue: any) {
    if (newValue === true) this.isRight = false;
  }

  public showTab(tabId: number) {
    this.activeTabId = tabId;

    this.emitEvent(ComponentEvents.ITEM_SELECT, {
      detail: {
        id: this.activeTabId,
      },
    });
  }

  protected get cssClasses(): string {
    const map = new Map([
      ['isRight', 'tabs-right'],
      ['isCentered', 'tabs-center'],
      ['grow', 'tabs-grow'],
    ]);
    return [...map.keys()]
      .filter((p) => this[p])
      .map((p) => map.get(p))
      .join(' ');
  }

  public static processTabs(compiler, resources, node: HTMLElement, instruction: BehaviorInstruction) {
    const headerTemplate = document.createElement('template');
    headerTemplate.setAttribute('replace-part', 'header');

    const contentTemplate = document.createElement('template');
    contentTemplate.setAttribute('replace-part', 'content');

    const tabs = Array.from(node.querySelectorAll('gt-tab'));
    for (let i = 0; i < tabs.length; i++) {
      const tab = tabs[i];

      const header = document.createElement('li');
      header.setAttribute('click.delegate', `showTab(${i})`);
      header.setAttribute('class.bind', `activeTabId===${i} && 'active'`);
      if (tab.hasAttribute('class')) {
        header.setAttribute('class', tab.getAttribute('class'));
      }

      const headerAnchor = document.createElement('a');
      headerAnchor.setAttribute('href', '#');
      headerAnchor.innerText = tab.getAttribute('header');

      header.appendChild(headerAnchor);
      headerTemplate.content.appendChild(header);

      const content = document.createElement('div');
      content.setAttribute('if.bind', `activeTabId===${i}`);
      content.append(...Array.from(tab.childNodes));
      contentTemplate.content.appendChild(content);

      node.removeChild(tab);
    }

    /*
    const bindingEngine: BindingEngine = Container.instance.get(BindingEngine);
    instruction.attributes = {
      ...instruction.attributes,
      'active-tab-id': bindingEngine.createBindingExpression('activeTabId', '0')
    };
    */

    node.append(headerTemplate, contentTemplate);
    return true;
  }
}
