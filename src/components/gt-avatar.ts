import { bindable } from 'aurelia-framework';

import { CoreComponent } from './base/core-component';

export class GtAvatar extends CoreComponent {
  @bindable url: string;
  @bindable size: number = 1;
  @bindable rounded: boolean = false;
  @bindable fallbackIcon: string = 'mdi-account-circle';

  sizeChanged(newValue: number, oldValue: any) {
    let value = newValue | 0;
    if (value > 7 || value < 1) this.size = 1;
    if (value !== newValue) this.size = value;
  }

  get cssClasses(): string {
    let classes = [];

    if (this.rounded) classes.push('round');
    if (this.size > 1 && this.size <= 7) classes.push(`avatar-${this.size}`);
    if (!this.url) classes.push('d-flex', 'flex-v-center', 'flex-h-center');

    return classes.join(' ');
  }

  get iconStyle() {
    const avatar = this.element?.querySelector('.avatar');
    const size = (avatar as HTMLElement)?.offsetWidth || 0;
    return `font-size: ${size}px;`;
  }
}
