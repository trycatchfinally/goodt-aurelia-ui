import { bindable } from 'aurelia-framework';

import { ComponentEvents } from '../constants';
import { SlotComponent } from './base/slot-component';

export class GtTree extends SlotComponent {
  static $resource = 'gt-tree';

  @bindable header: string;
  @bindable itemId: number;
  @bindable active: boolean = false;
  @bindable initialized: boolean = true;
  @bindable iconClass: string;
  @bindable labelClass: string;

  public isItemReady: boolean = false;
  public isCtrlReady: boolean = false;

  private $slots: any[];

  created(owningView: any, myView: any) {
    this.validSlot = GtTree;
    super.created(owningView, myView);
  }

  attached() {
    super.attached();
    this.$slots = (this.element as any)?.au?.controller.view.slots;
  }

  public itemClicked() {
    this.emitEvent(ComponentEvents.ITEM_SELECT, {
      detail: {
        id: this.itemId,
      },
    });
  }

  public ctrlClicked() {
    if (!this.initialized) return;
    this.active = !this.active;
    this.emitEvent(ComponentEvents.ITEM_CTRL_UPDATE, {
      detail: {
        id: this.itemId,
        status: this.active,
        initialized: this.initialized,
      },
    });
  }

  get slotChildrenLength() {
    const defaultSlotKey = Object.keys(this.$slots ?? {})?.[0];
    if (defaultSlotKey) {
      const children = this.$slots[defaultSlotKey].children;
      return (children as any[])?.filter((el) => !(el instanceof Comment))?.length ?? 0;
    }
    return 0;
  }

  get iconCssClass() {
    let classes = [];

    if (this.iconClass) classes.push(this.iconClass);
    classes.push(this.initialized ? (this.active ? 'mdi-chevron-down' : 'mdi-chevron-right') : 'mdi-loading mdi-spin');
    if (this.isCtrlReady) classes.push('color-primary');

    return classes.join(' ');
  }

  get itemCssClass() {
    let classes = [];

    if (this.labelClass) classes.push(this.labelClass);
    if (this.isItemReady) classes.push('color-primary cursor-pointer');
    return classes.join(' ');
  }
}
