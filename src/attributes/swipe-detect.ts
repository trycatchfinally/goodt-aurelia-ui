import { autoinject } from 'aurelia-framework';

import { mouseDown, mouseOut, mouseUp, touchDevice } from '../components/base/helpers';
import { SwipeDirection } from '../constants';

/**
 * Swipe detect for devices with touch support
 */
@autoinject()
export class SwipeDetectCustomAttribute {
  public value: any; // callback function

  private touchStartBound: any;
  private touchEndBound: any;

  private startX: number;
  private startY: number;

  private timeStart: number;
  private timer: any;

  private delay: number = 250;
  private minDelta: number = 30;

  private delayedTarget: EventTarget;

  constructor(private element: Element) {
    if (touchDevice) {
      this.touchStartBound = this.touchStartHandler.bind(this);
      this.touchEndBound = this.touchEndHandler.bind(this);

      this.reset();
    }
  }

  bind() {
    if (touchDevice) {
      this.element.addEventListener(mouseDown, this.touchStartBound, false);
      this.element.addEventListener(mouseOut, this.touchEndBound, false);
      this.element.addEventListener(mouseUp, this.touchEndBound, false);
    }
  }

  unbind() {
    if (touchDevice) {
      this.element.removeEventListener(mouseDown, this.touchStartBound, false);
      this.element.removeEventListener(mouseOut, this.touchEndBound, false);
      this.element.removeEventListener(mouseUp, this.touchEndBound, false);
    }
  }

  private touchStartHandler = (e: TouchEvent) => {
    this.delayedTarget = e.target;
    e.preventDefault();

    this.startX = e.changedTouches[0].screenX;
    this.startY = e.changedTouches[0].screenY;
    this.timeStart = new Date().getTime();

    this.timer = setTimeout(this.reset, this.delay);
  };

  private touchEndHandler = (e: TouchEvent) => {
    if (new Date().getTime() - this.timeStart >= this.delay) return true;
    clearTimeout(this.timer);

    const diffX = e.changedTouches[0].screenX - this.startX;
    const diffY = e.changedTouches[0].screenY - this.startY;
    const ratioX = Math.abs(diffX / diffY);
    const ratioY = Math.abs(diffY / diffX);
    const absDiff = Math.abs(ratioX > ratioY ? diffX : diffY);

    if (absDiff < this.minDelta) {
      this.resetoreDelayedEvent();
      return true;
    }

    let direction: SwipeDirection =
      ratioX > ratioY
        ? diffX >= 0
          ? SwipeDirection.RIGHT
          : SwipeDirection.LEFT
        : diffY >= 0
        ? SwipeDirection.DOWN
        : SwipeDirection.UP;

    this.value.call(e, direction);
    this.reset();

    return false;
  };

  private resetoreDelayedEvent = () => {
    if (!this.delayedTarget) return;

    let e = new Event('click');
    this.delayedTarget.dispatchEvent(e);
  };

  private reset = () => {
    this.startX = 0;
    this.startY = 0;
    this.timeStart = 0;

    this.timer = null;
    this.delayedTarget = null;
  };
}
