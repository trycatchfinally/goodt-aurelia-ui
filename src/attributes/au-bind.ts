import { autoinject, BindingLanguage, Container, TargetInstruction } from 'aurelia-framework';

const APPLY_TARGET_ONLY = 'au-bind-target';

// delegated bindings, see https://github.com/aurelia/aurelia/issues/319
@autoinject()
export class AuBindCustomAttribute {
  expressions: any[];
  bindings: any[];
  bindingLanguage: BindingLanguage;
  owningView: any;
  targetInstruction: any;

  private targetOnly: boolean = false;

  constructor(private element: Element, container: Container, bindingLanguage: BindingLanguage) {
    this.element = element;
    this.targetInstruction = container.parent.get(TargetInstruction);

    let expressions = this.targetInstruction.expressions;
    let parentAttributes =
      Object.values({
        ...(container.parent.get(Element).attributes as NamedNodeMap),
      }).map((i: Attr) => i.localName) ?? [];

    this.targetOnly = parentAttributes.includes(APPLY_TARGET_ONLY);
    if (this.targetOnly) this.targetInstruction.expressions = [];

    this.expressions = expressions;
    this.bindingLanguage = bindingLanguage;
  }

  created(owningView: any) {
    this.owningView = owningView;
  }

  bind() {
    if (!this.bindings) {
      this.bindings = this.expressions
        .map((e) => {
          if (e?.targetProperty) {
            e.targetProperty = (this.bindingLanguage as any)?.attributeMap.map(
              this.element?.tagName?.toLowerCase(),
              e.targetProperty,
            );
            return e.createBinding(this.element);
          }
          return false;
        })
        .filter((b) => b);
    }
    this.bindings?.forEach((b) => b.bind(this.owningView.bindingContext.owningView));
  }

  unbind() {
    if (this.targetOnly) this.targetInstruction.expressions = this.expressions;
    this.bindings?.forEach((b) => b.unbind());
  }
}
