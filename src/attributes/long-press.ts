import { autoinject } from 'aurelia-framework';

import { mouseDown, mouseOut, mouseUp } from '../components/base/helpers';

// wheel/scroll events
const mouseWheel: string = 'mousewheel';
const wheel: string = 'wheel';
const scrollEvent: string = 'scroll';

// based on https://github.com/john-doherty/long-press-event
@autoinject()
export class LongPressCustomAttribute {
  public value: any; // callback function

  private timer: any;
  private interval: any;

  private delay: number = 500;
  private duration: number = 50;

  private timerInitBound: any;
  private clearDelaysBound: any;

  constructor(private element: Element) {
    this.timerInitBound = this.timerInit.bind(this);
    this.clearDelaysBound = this.clearDelays.bind(this);
  }

  bind() {
    // start long press
    this.element.addEventListener(mouseDown, this.timerInitBound);

    // finish long press
    this.element.addEventListener(mouseUp, this.clearDelaysBound);
    this.element.addEventListener(mouseOut, this.clearDelaysBound);
    this.element.addEventListener(mouseWheel, this.clearDelaysBound);
    this.element.addEventListener(wheel, this.clearDelaysBound);
    this.element.addEventListener(scrollEvent, this.clearDelaysBound);
  }

  unbind() {
    this.element.removeEventListener(mouseUp, this.clearDelaysBound);
    this.element.removeEventListener(mouseOut, this.clearDelaysBound);
    this.element.removeEventListener(mouseWheel, this.clearDelaysBound);
    this.element.removeEventListener(wheel, this.clearDelaysBound);
    this.element.removeEventListener(scrollEvent, this.clearDelaysBound);

    this.element.removeEventListener(mouseDown, this.timerInitBound);
    this.clearDelays();
  }

  private timerInit = (e: Event) => {
    this.timer = setTimeout(() => {
      this.intervalInit(e);
    }, this.delay);
  };

  private intervalInit = (e: Event) => {
    this.interval = setInterval(() => {
      this.value.call(e);
    }, this.duration);
  };

  private clearDelays = () => {
    if (this.interval) clearInterval(this.interval);
    if (this.timer) clearTimeout(this.timer);
    this.interval = null;
    this.timer = null;
  };
}
