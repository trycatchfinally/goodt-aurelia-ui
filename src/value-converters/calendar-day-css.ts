import { formatDateLabel } from '../components/base/helpers';
import { ICalendarDay, ICalendarEmpty } from '../interfaces';

export class CalendarDayCssValueConverter {
  toView(day: ICalendarDay | ICalendarEmpty, date: Date = null) {
    if (day.empty) return 'events-none';

    let classes: string[] = [];
    let isToday: boolean = formatDateLabel(new Date()) === formatDateLabel((day as ICalendarDay).value);

    if ((day as ICalendarDay).disabled) {
      classes.push('events-none');

      isToday ? classes.push('bg-disabled', 'color-primary') : classes.push('color-disabled-dark');

      return [...new Set(classes)].join(' ');
    }

    date && formatDateLabel((day as ICalendarDay).value) === formatDateLabel(date)
      ? classes.push('bg-primary', 'color-white', 'events-none')
      : classes.push('cursor-pointer');

    if (isToday && !classes.some((_) => _ === 'color-white')) classes.push('color-primary');

    return [...new Set(classes)].join(' ');
  }
}
