export class NormalizeNumeralValueConverter {
  toView(value: string): string | number {
    let parsed = parseInt(value);
    return isNaN(parsed) ? value : parsed;
  }
}
