import { INameValue } from '../interfaces';

export class PresetCssValueConverter {
  toView(preset: INameValue, value: string): string {
    return preset.disabled ? 'events-none color-disabled-dark' : preset.name === value ? 'active events-none' : null;
  }
}
