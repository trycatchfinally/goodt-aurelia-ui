const smoothScrollHeight: number = 480;

const components: string[] = [
  'alert',
  'avatar',
  'checkbox',
  'chip',
  'date-picker',
  'details',
  'dropdown',
  'grid',
  'grid-column',
  'menu',
  'number-field',
  'off-canvas',
  'popup',
  'preloader',
  'radio',
  'select',
  'shim',
  'switch',
  'tabs',
  'time-picker',
  'toast',
  'tree',
];

enum ErrorsCodes {
  INVALID_VALUE,
  MIN_MAX,
  OUT_OF_RANGE,
  NOT_EXCLUDED,
  INVALID_TYPE,
}

enum ComponentEvents {
  VALUE_CHANGED = 'value-changed',
  ERROR_DETECTED = 'error-detected',
  DEACTIVATE_READY = 'deactivate-ready',
  ITEM_SELECT = 'item-select',
  ITEM_CTRL_UPDATE = 'item-ctrl-update',
}

enum SwipeDirection {
  UP = 'up',
  DOWN = 'down',
  LEFT = 'left',
  RIGHT = 'right',
}

export { components, smoothScrollHeight, ErrorsCodes, ComponentEvents, SwipeDirection };
