import {inject} from 'aurelia-dependency-injection';
import {Project, ProjectItem, CLIOptions, UI} from 'aurelia-cli';

@inject(Project, CLIOptions, UI)
export default class ElementGenerator {
  constructor(private project: Project, private options: CLIOptions, private ui: UI) { }

  async execute() {
    const name = await this.ui.ensureAnswer(
      this.options.args[0],
      'What would you like to call the custom element?'
    );

    let fileName = `gt-${this.project.makeFileName(name)}`;
    let className = this.project.makeClassName(name);

    this.project.elements.add(
      ProjectItem.text(`${fileName}.ts`, this.generateJSSource(className)),
      ProjectItem.text(`${fileName}.html`, this.generateHTMLSource(className))
    );

    await this.project.commitChanges();
    await this.ui.log(`Created ${fileName}.`);
  }

  generateJSSource(className) {
    return `import {bindable} from 'aurelia-framework'
import {SlotComponent} from './base/slot-component'

export class Gt${className} extends SlotComponent {

  @bindable value = 'Gt${className}';

}
`;
  }

  generateHTMLSource(className) {
    return `<template>
  <div class="color-red">TBD \${value}</div>
</template>
`;
  }
}
