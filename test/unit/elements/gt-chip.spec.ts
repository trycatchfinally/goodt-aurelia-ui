import {StageComponent} from 'aurelia-testing';
import {bootstrap} from 'aurelia-bootstrapper';

describe('gt-chip element', () => {
  let component;

  afterEach(() => {
    if (component) {
      component.dispose();
      component = null;
    }
  });

  it('must be rounded', done => {
    let model = {rounded: true};

    component = StageComponent
      .withResources('resources/components/gt-chip')
      .inView('<gt-chip rounded.bind="rounded"></gt-chip>')
      .boundTo(model);

    component.create(bootstrap).then(() => {
      const view = component.element;
      expect(view.classList.toString()).toContain('round');
      done();
    }).catch(e => {
      fail(e);
      done();
    });
  });
});
