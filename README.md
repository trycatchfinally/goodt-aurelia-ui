# `goodt-aurelia-ui`

## Plugin initialization
In order to tell Aurelia how to use the plugin, we need to register it.
This is done in your app's ```main``` file, specifically the ```configure``` method:
```js
// main.js
import { Aurelia } from 'aurelia-framework';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources');

    aurelia.use.plugin('goodt-aurelia-ui');

    aurelia.start().then(() => aurelia.setRoot());
}
```

## Docs and demo
* [Get started](https://goodt-aurelia-ui.web.app/)
* Extended [documentation and examples](https://goodt-aurelia-ui.web.app/#/components)
* Brief description for [properties and events](https://goodt-aurelia-ui.web.app/#/cheat-sheet)
